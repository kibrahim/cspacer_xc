#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.69])
AC_INIT(CSPACER, 0.1, kzibrahim@lbl.gov)
AM_INIT_AUTOMAKE

AC_CONFIG_HEADER([config.h])

AC_PROG_CC([gcc cl cc])

AC_CONFIG_MACRO_DIR([m4])

AC_C_RESTRICT
AC_PROG_CC_C99
AC_OPENMP


# Checks for programs.
m4_ifdef([AM_PROG_AR], [AM_PROG_AR])
AC_PROG_CC
AM_PROG_AS
AM_PROG_LIBTOOL

# Checks for libraries.

AC_CHECK_LIB([pthread], [main])

# Checks for header files.

AC_HEADER_STDC
AC_CHECK_HEADERS([fcntl.h malloc.h stdint.h stdlib.h string.h sys/ioctl.h unistd.h values.h linux/mempolicy.h])
AC_CHECK_DECLS([__NR_mbind], [], [], [[#include <sys/syscall.h>]])


test -z "$SED" && SED=sed

AC_ARG_ENABLE([debug],
  [AS_HELP_STRING([--enable-debug],
                  [whether to include debug symbols (default is no)])],
  [enable_debug=$enableval],
  [enable_debug=no]
)

if test "x$enable_debug" = xyes; then
  dnl Remove all optimization flags from CFLAGS
  changequote({,})
  CFLAGS=`echo "$CFLAGS" | $SED -e 's/-O[0-9s]*//g'`
  CXXFLAGS=`echo "$CXXFLAGS" | $SED -e 's/-O[0-9s]*//g'`

  CFLAGS=`echo "$CFLAGS" | $SED -e 's/-g[0-9]*//g'`
  CXXFLAGS=`echo "$CXXFLAGS" | $SED -e 's/-g[0-9]*//g'`
  changequote([,])

  CFLAGS="$CFLAGS -g -O0"
  CXXFLAGS="$CXXFLAGS -g -O0"
fi

echo "CFLAGS=$CFLAGS"


# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_C_INLINE
AC_TYPE_SIZE_T
AC_C_VOLATILE


# Checks for library functions.
AC_PROG_GCC_TRADITIONAL
AC_FUNC_MALLOC
AC_FUNC_MEMCMP
AC_FUNC_MMAP
AC_FUNC_VPRINTF
AC_CHECK_FUNCS([memset strerror])

PKG_PROG_PKG_CONFIG

AC_ARG_ENABLE([pmi],
  [AS_HELP_STRING([--enable-pmi=[yes|no]],
     [use pmi for tests (default is yes)])],
  [case "${enableval}" in
     no) 
       pmi=no
       ;;
     yes)
       pmi=yes
       ;;
     *)  
       AC_MSG_ERROR([--enable-pmi arguments restricted to yes or no. Got ${enableval}])
       ;;
   esac],
  [pmi=yes])

PKG_CHECK_MODULES([CRAY_PMI], [cray-pmi])

PKG_CHECK_MODULES([CRAY_XPMEM], [cray-xpmem])


AC_ARG_WITH([gni-arch],
  [AS_HELP_STRING([--with-gni-arch=[gem|ari]],
     [use to set GNI network type.])],
  [case "${withval}" in
     gem)
  AC_SUBST([CRAY_CONFIG_GHAL_PLATFORM], [CRAY_CONFIG_GHAL_GEMINI])
  AM_CONDITIONAL([GNI_ARCH_CONFIG_ARIES], [false])
  AC_SUBST([GNI_ARCH_CONFIG_FLAG], [CONFIG_GEMINI])
        ;;  
     ari)
  AC_SUBST([CRAY_CONFIG_GHAL_PLATFORM], [CRAY_CONFIG_GHAL_ARIES])
  AM_CONDITIONAL([GNI_ARCH_CONFIG_ARIES], [true])
  AC_SUBST([GNI_ARCH_CONFIG_FLAG], [CONFIG_ARIES])
       ;;  
     *)  
       AC_MSG_ERROR([--with-gni-arch=${withval} invalid.])
       ;;  
   esac],
  [AC_MSG_ERROR([--with-gni-arch required.])])

AC_CHECK_LIB([numa],[numa_max_node])
AC_CHECK_LIB([xpmem],[xpmem_make])


AC_PREFIX_DEFAULT(/opt/cspacer)

AC_OUTPUT(Makefile src/Makefile examples/Makefile utils/Makefile include/Makefile)



