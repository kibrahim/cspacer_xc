/*------------------------------------------------------------------------------
 * Description: CSPACER Implemention of an irregular alltoall pattern.
 *              The alltoallv is utilizing application threads to accelerate 
 *              the injection.
 * Author: Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/


#include "cspacer.h"
#include "cspacer_pattern_utils.h"
#include "cspacer_profile.h"
#include "assert.h"
#include "omp.h"

#include <unistd.h>
#include <sys/time.h>

#define DEF_LOG_ELEMENT_COUNT   23
#define RESIDUAL_ELEMENT    129
#define INJECTION_SPACER    0
#define INPLACE_REDUCTION   0
#define VERIFY_SUBSET       0
#define MAX_EXTRA_CHUNKS    8

cs_profile_info_t cs_profile_info;

int main(int argc, char *argv[])
{
  int32_t channel_count, lane_count, team_count,i,j, col_rank,row_rank, rank, partner, chunk;
  int32_t col_team_size, row_team_size, univ_team_size;
  cspacer_space_handle_t src_cspace_hndl, src_meta_cspace_hndl, rmt_cspace_hndl, rmt_meta_cspace_hndl;
  int32_t element_count, extra_chunks, extra_elements, chunk_count, dest_element_count,
          meta_element_count, max_chunk_per_rank, per_rank_element_count;
  uint64_t start_cycle, end_cycle, total_cycles=0, meta_sync_cycles;
  uint64_t space_size, dest_space_size, meta_space_size;
  int nap_time = 1000;
  struct timeval start, end;
  double exchanged_bytes, time_ms, total_time_ms, bw;
  uint64_t *src_space_ptr, *src_meta_space_ptr,  *dest_space_ptr, *dest_meta_space_ptr;
  int32_t ITER, iter;
  int64_t log_element_count;

  cspacer_transfer_handle_t *tx_handles;
  cspacer_reduction_handle_t *tx_meta_handle;
  cspacer_lane_handle_t *lanes; 
  cspacer_channel_handle_t *channels;
  cspacer_team_handle_t h_self_team, h_col_team, h_row_team, h_univ_team;

  char *config_file_name;
  cspacer_return_t ret;

  parse_options(argc,argv);

  chunk = cspacer_options.chunk_size;
  log_element_count = cspacer_options.log_message_size?cspacer_options.log_message_size:DEF_LOG_ELEMENT_COUNT;
  ITER = cspacer_options.iteration_count;
  config_file_name = cspacer_options.config_file;

  ret = cspacer_init_runtime(argc,argv,config_file_name); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_lane_count(&lane_count); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_channel_count(&channel_count); CHECK_CSPACE_RET(ret);

  ret = cspacer_get_team_handle(CSPACER_SELF_TEAM,   &h_self_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_COLUMN_TEAM, &h_col_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_ROW_TEAM,    &h_row_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_UNIVERSAL_TEAM, &h_univ_team); CHECK_CSPACE_RET(ret);

  ret = cspacer_team_size(h_row_team, &row_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_col_team, &col_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_univ_team, &univ_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_col_team, &col_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_row_team, &row_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_univ_team, &rank); CHECK_CSPACE_RET(ret);
  lanes = malloc(sizeof(cspacer_lane_handle_t)*lane_count);

  srand(rank); 
//  fprintf(stderr,"Rank %d: r %d c %d\n", rank, row_rank,col_rank);
  extra_chunks = rand()%MAX_EXTRA_CHUNKS;
  extra_elements = rand()%chunk;
  chunk_count = (1 << log_element_count)/chunk;
  max_chunk_per_rank = (chunk_count + MAX_EXTRA_CHUNKS+ col_team_size-1)/col_team_size;
  element_count = (chunk_count + extra_chunks) * chunk + extra_elements;
  per_rank_element_count = max_chunk_per_rank*chunk;
  
  dest_element_count = per_rank_element_count * col_team_size;
  meta_element_count = col_team_size*col_team_size;
  space_size = element_count*sizeof(uint64_t);
  dest_space_size = dest_element_count*sizeof(uint64_t);
  meta_space_size = meta_element_count*sizeof(uint64_t);  

  channels = malloc(sizeof(cspacer_channel_handle_t)*channel_count);
  tx_handles = malloc(sizeof(cspacer_transfer_handle_t)*lane_count*col_team_size);
  // We will allow gaps in the src ptr. 
  ret = cspacer_sym_allocate_memory(h_col_team, dest_space_size,CSPACER_DEFAULT_SPACE, &rmt_cspace_hndl); CHECK_CSPACE_RET(ret);
//  ret = cspacer_reset_state(rmt_cspace_hndl,0); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(rmt_cspace_hndl, (void **)&dest_space_ptr); CHECK_CSPACE_RET(ret);

  ret = cspacer_sym_allocate_memory(h_col_team, lane_count*meta_space_size,CSPACER_REDUCTION64_SPACE, &rmt_meta_cspace_hndl); CHECK_CSPACE_RET(ret);
//  ret = cspacer_reset_state(rmt_meta_cspace_hndl,0); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(rmt_meta_cspace_hndl, (void **)&dest_meta_space_ptr); CHECK_CSPACE_RET(ret);

  ret = cspacer_sym_allocate_memory(h_self_team, space_size,CSPACER_DEFAULT_SPACE, &src_cspace_hndl); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(src_cspace_hndl, (void **)&src_space_ptr); CHECK_CSPACE_RET(ret);

#if INPLACE_REDUCTION
  src_meta_cspace_hndl = rmt_meta_cspace_hndl;
  src_meta_space_ptr = dest_meta_space_ptr;
#else
  ret = cspacer_sym_allocate_memory(h_self_team, lane_count*meta_space_size,CSPACER_DEFAULT_SPACE, &src_meta_cspace_hndl); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(src_meta_cspace_hndl, (void **)&src_meta_space_ptr); CHECK_CSPACE_RET(ret);
#endif

  #pragma omp parallel for
  for(i=0;i<dest_element_count;i++)
    dest_space_ptr[i] = 0; 
  #pragma omp parallel for
  for(i=0;i<element_count;i++)
    src_space_ptr[i] = 0; 



  int max_threads = omp_get_max_threads();
  int first_level_omp_threads = MIN(max_threads, lane_count);
  lane_count = first_level_omp_threads;
  cs_exmpl_init_profile_info(lane_count);
  int second_level_omp_threads = max_threads/first_level_omp_threads; 
  cs_exmpl_init_profile_info(max_threads);
  assert(space_size/chunk/first_level_omp_threads >0);

  for(i=0;i<lane_count;i++) {
    ret=cspacer_get_lane_handle(i, lanes+i); CHECK_CSPACE_RET(ret);
    for(j=0;j<col_team_size;j++) {
      ret =  cspacer_construct_put(rmt_cspace_hndl, 0, j, 
                          src_cspace_hndl, 0, chunk*sizeof(uint64_t),
                          lanes[i], tx_handles+i*col_team_size+j);  CHECK_CSPACE_RET(ret);
    }
  }

  ret = cspacer_get_channel_handle(0, channels+0); CHECK_CSPACE_RET(ret);
  tx_meta_handle = malloc(sizeof(cspacer_reduction_handle_t)*channel_count);

  cspacer_operation_t op = CSPACER_ADD_U64;
  cspacer_reduction_algorithm_t pattern = CSPACER_RED_SCATTER_BROADCAST_INORDER;
  ret = cspacer_construct_reduction (rmt_meta_cspace_hndl,op,channels[0], tx_meta_handle+0);  CHECK_CSPACE_RET(ret);
  ret = cspacer_plan_reduction(meta_element_count,CSPACER_DEFAULT_REDUCTION_CHUNK,
          CSPACER_DEFAULT_FLUSH_DEPTH,pattern,INPLACE_REDUCTION, tx_meta_handle[0]); CHECK_CSPACE_RET(ret);
  cspacer_declare_team_helper_threads(h_col_team, first_level_omp_threads);
  src_meta_space_ptr += col_team_size*col_rank;

  #pragma omp parallel num_threads(first_level_omp_threads)
  {
    int32_t nthreads = omp_get_num_threads();
    int32_t tid = omp_get_thread_num();
    cspacer_warm_space_threaded(rmt_meta_cspace_hndl,tid,nthreads);
  }
  cspacer_barrier();

  gettimeofday(&start, NULL);
#if TIME_MARKER
  cs_exmpl_record_time_marker(0,0/*tid*/, start_cycle);
#endif
  for(iter = 0; iter<ITER; iter++) {
    start_cycle = read_cpu_cycles();


    #pragma omp parallel num_threads(first_level_omp_threads)
    {
      cspacer_return_t thr_ret;
      int32_t nthreads = omp_get_num_threads();
      int32_t tid = omp_get_thread_num();
      int32_t tx_count, t, ti, target_rank, rank_dest_start;
      int64_t assignment, start,end, thr_start_cycle, thr_end_cycle, thr_start, thr_dest_start;
      uint64_t *meta_src_ptr =  src_meta_space_ptr + tid * meta_element_count;
 
      cspacer_lane_handle_t mylane = lanes[tid];
      cspacer_transfer_handle_t *tx_hndls = tx_handles+tid*col_team_size;
      tx_count = (element_count + chunk-1)/chunk;
      rank_dest_start = col_rank * per_rank_element_count * sizeof(uint64_t);
#if TIME_MARKER
      cs_exmpl_record_time_marker(1,tid, read_cpu_cycles());
#endif
      cs_exmpl_record_thread_binding(0, tid);
      #pragma omp for
      for(i=0;i<meta_element_count*lane_count;i++)
        src_meta_space_ptr[i] = 0;


      for(t=tid;t<tx_count;t+=nthreads) {
         /* 
          * We should be able to do computation with the full level of parallelism
          */
          start = t*chunk;
          end = (t+1)*chunk;
          if(end>element_count) end = element_count;
          if(assignment<0) continue;
          assignment = end - start;
          target_rank = (t+col_rank) % col_team_size;
          if(target_rank == col_rank || assignment <= 0)
            continue;
          for(ti=start;ti<end;ti++)
            src_space_ptr[ti] = rank+ti+1;
          thr_start = start * sizeof(uint64_t);
          assignment *= sizeof(uint64_t);
          thr_dest_start = rank_dest_start + (t/col_team_size)*chunk*sizeof(uint64_t); 
          thr_ret = cspacer_update_transfer(thr_dest_start, thr_start, assignment, tx_hndls[target_rank]);  CHECK_CSPACE_RET(thr_ret);
          thr_start_cycle = read_cpu_cycles();
          while((thr_ret = cspacer_initiate_transfer(tx_hndls[target_rank])) != CSPACER_SUCCESS) {
            thr_end_cycle = read_cpu_cycles();
            cs_exmpl_record_profile_info(t,tid, CS_ATTEMPT_TRANSFER, thr_end_cycle-thr_start_cycle);
            thr_start_cycle = thr_end_cycle;//read_cpu_cycles();
          }
          thr_end_cycle = read_cpu_cycles();
          cs_exmpl_record_profile_info(t,tid, CS_INITIATE_TRANSFER, thr_end_cycle-thr_start_cycle);
//          __sync_fetch_and_add(meta_src_ptr+target_rank, assignment);
          meta_src_ptr[target_rank]+= assignment;
        #if INJECTION_SPACER
          usleep(nap_time);
        #endif
      }
      /*We cannot free transfer before flushing the lane it is used for */
      thr_start_cycle = read_cpu_cycles();
      cspacer_flush_lane(mylane);
      thr_end_cycle = read_cpu_cycles();
      cs_exmpl_record_profile_info(0,tid, CS_FLUSH_LANE, thr_end_cycle-thr_start_cycle);
    }
    end_cycle = read_cpu_cycles();
    total_cycles = end_cycle - start_cycle;
    start_cycle = read_cpu_cycles();
    for(j=1;j<first_level_omp_threads;j++) {
      for(i=0;i<col_team_size;i++)
      src_meta_space_ptr[i] += src_meta_space_ptr[i+meta_element_count*j];
    }
    #if COORDINATED_IMPLICIT_HANDLE
    ret = cspacer_deposit_reduction_chunk(
             rmt_meta_cspace_hndl, src_meta_cspace_hndl, 0, meta_element_count); CHECK_CSPACE_RET(ret);
    #else
    ret = cspacer_deposit_reduction_chunk(
             src_meta_cspace_hndl, 0, meta_element_count, tx_meta_handle[0]); CHECK_CSPACE_RET(ret);
    #endif
    ret = cspacer_wait_consistent(rmt_meta_cspace_hndl,meta_element_count*sizeof(uint64_t));  CHECK_CSPACE_RET(ret);
    uint64_t tag = 0;
    for(i=0;i<col_team_size;i++)
      tag += dest_meta_space_ptr[col_rank+i*col_team_size];
    end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(0,0, CS_WAIT_METADATA,end_cycle-start_cycle);
    total_cycles += end_cycle - start_cycle;

    start_cycle = read_cpu_cycles();
    cspacer_wait_consistent(rmt_cspace_hndl,tag);
    end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(0,0, CS_WAIT_CONSISTENT,end_cycle-start_cycle);
    total_cycles += end_cycle - start_cycle;
    if(iter<ITER-1) {
      cspacer_barrier();
      cspacer_reset_state(rmt_cspace_hndl,CSPACER_RESET_INIT);
      cspacer_reset_state(rmt_meta_cspace_hndl,CSPACER_RESET_INIT);
      ret = cspacer_plan_reduction(meta_element_count,CSPACER_DEFAULT_REDUCTION_CHUNK,
          CSPACER_DEFAULT_FLUSH_DEPTH,pattern,INPLACE_REDUCTION, tx_meta_handle[0]); CHECK_CSPACE_RET(ret);
      cspacer_barrier();
      if(iter == 0) {
        cs_exmpl_init_profile_info(lane_count);
        gettimeofday(&start, NULL);
        total_cycles = 0;
      }
    }
  }
  gettimeofday(&end, NULL);

//  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);

  cspacer_barrier();
  if(ITER>1) ITER--;

  time_ms = ((double) (end.tv_sec - start.tv_sec) * 1e3 + end.tv_usec/1e3) - (double)start.tv_usec / 1e3;

  total_time_ms = time_ms;
  cspacer_hw_reduction(CSPACER_ADD_FP64, &total_time_ms);
  time_ms = total_time_ms/univ_team_size; /* Average across all ranks */

  exchanged_bytes = 0;
  for(i=0;i<col_team_size;i++)
    for(j=0;j<col_team_size;j++)
      exchanged_bytes+= dest_meta_space_ptr[i*col_team_size+j];

  bw = row_team_size*exchanged_bytes/(time_ms*1e3)*ITER;
//  cspacer_free_reduction(tx_meta_handle[0]);

  if(rank==PROFILE_RANK)
    fprintf(stderr,"Problem setup:\n"
        " Irregular alltoallv across column, Maximum threads: %d\n"
        "    First level %d second level %d\n"
        "    Team: r %d x c %d\n"
        " Data size: %d, chunk %d words, Iter %d\n"
        " Rank count: %d\n"
        " Time: %f ms\n"
        " BW: %f MB/s\n",
        max_threads,
        first_level_omp_threads, second_level_omp_threads,
        row_team_size,col_team_size,
        space_size, chunk, ITER,
        univ_team_size,
        time_ms/ITER,
        bw);

  ret = cspacer_free_reduction(tx_meta_handle[0]); CHECK_CSPACE_RET(ret);
  for(i=0;i<lane_count;i++) {
   for(j=0;j<col_team_size;j++) {
     ret = cspacer_free_transfer(tx_handles[i*col_team_size+j]); CHECK_CSPACE_RET(ret);
   }
  }


  if(rank==PROFILE_RANK)
    fprintf(stderr,"Start Checking results:...\n");
  uint32_t team_rank, global_rank, first_chunk;
  for(team_rank=0;team_rank<col_team_size;team_rank++) {
    uint32_t word_count = dest_meta_space_ptr[col_rank+team_rank*col_team_size]/sizeof(uint64_t);
    if(word_count == 0)
      continue;
    global_rank = team_rank * row_team_size + row_rank;
    for(first_chunk=0;first_chunk<col_team_size;first_chunk++)
      if((first_chunk+team_rank) % col_team_size == col_rank)
        break;
    uint64_t * check_ptr = dest_space_ptr + team_rank*per_rank_element_count;
    #pragma omp parallel for
    for(i=0;i<word_count;i++) 
    {
        uint32_t cur_chunk = i/chunk;
        uint32_t offset = i%chunk;
        uint64_t expected = global_rank+ offset +1 + (cur_chunk*col_team_size + first_chunk)*chunk;
        if(check_ptr[i] != expected)
          fprintf(stderr,"Rank %d (team id %d from %d) error at pos %d found %"PRIu64" expected %"PRIu64"\n", rank, row_rank, team_rank, i, check_ptr[i], expected);  
    }
  }

  if(rank==PROFILE_RANK) {
    fprintf(stderr,"finished checking\n");
    cs_exmpl_record_profile_global(0,end_cycle-start_cycle,total_cycles, ITER);
    cs_example_dump_profile_info(rank);
  }
 // fprintf(stderr, "tag at rank %d is %lld\n", rank, tag);


  cspacer_fini_runtime();
  return 0;
}

