/*------------------------------------------------------------------------------
 * Description: CSPACER Implemention of a reduction pattern.
 *              The reduction is utilizing application threads to accelerate 
 *              the prcoessing.
 * Author: Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/


#include "cspacer.h"
#include "cspacer_pattern_utils.h"
#include "cspacer_profile.h"
#include "assert.h"
#include "omp.h"

#include <unistd.h>
#include <sys/time.h>

#define DEF_LOG_ELEMENT_COUNT   23
#define RESIDUAL_ELEMENT    128
#define INJECTION_SPACER    0
#define MULTI_THREADED_WAIT 1
#define INPLACE_REDUCTION   0
#define SPARSE_INPUT        (0 && (!INPLACE_REDUCTION))
#define VERIFY_SUBSET       0
#define FP_REDUCTION        0
cs_profile_info_t cs_profile_info;

int main(int argc, char *argv[])
{
  int32_t channel_count, team_count,i,j, col_rank,row_rank, rank, partner, chunk, leader;
  int32_t col_team_size, row_team_size, univ_team_size, leader_sync;
  cspacer_space_handle_t src_cspace_hndl, rmt_cspace_hndl;
  int32_t element_count;
  uint64_t start_cycle, end_cycle, total_cycles=0;
  uint64_t space_size;
  int nap_time = 1000,iter;
  struct timeval start, end;
  double exchanged_bytes, time_ms, total_time_ms, bw;
  uint64_t * src_space_ptr, *dest_space_ptr;
  int32_t ITER;
  cspacer_reduction_handle_t *tx_handles;
  cspacer_channel_handle_t *channels;
  cspacer_team_handle_t h_self_team, h_col_team, h_row_team, h_univ_team;
  int64_t log_element_count;

  char *config_file_name;
  cspacer_return_t ret;
  cspacer_sync_mode_t sync;

  parse_options(argc,argv);
  chunk = cspacer_options.chunk_size;
  log_element_count = cspacer_options.log_message_size?cspacer_options.log_message_size:DEF_LOG_ELEMENT_COUNT;
  ITER = cspacer_options.iteration_count;
  config_file_name = cspacer_options.config_file;
  leader_sync = cspacer_options.leader_sync;


  ret = cspacer_init_runtime(argc,argv,config_file_name); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_channel_count(&channel_count); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_SELF_TEAM,   &h_self_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_COLUMN_TEAM, &h_col_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_ROW_TEAM,    &h_row_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_UNIVERSAL_TEAM, &h_univ_team); CHECK_CSPACE_RET(ret);

  ret = cspacer_team_size(h_row_team, &row_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_col_team, &col_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_univ_team, &univ_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_col_team,&col_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_row_team,&row_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_univ_team,&rank); CHECK_CSPACE_RET(ret);
//  fprintf(stderr,"Rank %d: r %d c %d\n", rank, row_rank,col_rank);

  element_count = (1 << log_element_count) + RESIDUAL_ELEMENT+chunk;
  space_size = element_count*sizeof(uint64_t);
  

  channels = malloc(sizeof(cspacer_channel_handle_t)*channel_count);
  tx_handles = malloc(sizeof(cspacer_reduction_handle_t)*channel_count);
  // We will allow gaps in the src ptr. 
  ret = cspacer_sym_allocate_memory(h_col_team, space_size,CSPACER_REDUCTION64_SPACE, &rmt_cspace_hndl); CHECK_CSPACE_RET(ret);
//  ret = cspacer_reset_state(rmt_cspace_hndl,0); CHECK_CSPACE_RET(ret);

  ret = cspacer_get_space_ptr(rmt_cspace_hndl, (void **)&dest_space_ptr); CHECK_CSPACE_RET(ret);

#if INPLACE_REDUCTION
  src_cspace_hndl = rmt_cspace_hndl;
  src_space_ptr = dest_space_ptr;
  sync = CSPACER_NO_SYNC;
#else
  ret = cspacer_sym_allocate_memory(h_self_team, 2*space_size,CSPACER_DEFAULT_SPACE, &src_cspace_hndl); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(src_cspace_hndl, (void **)&src_space_ptr); CHECK_CSPACE_RET(ret);
  sync = CSPACER_ALL_SYNC;
#endif

  #pragma omp parallel for
  for(i=0;i<element_count;i++)
    dest_space_ptr[i] = 0; //correct value should be: col_rank*col_team_size + (element_count-1)*(element_count)/2

  exchanged_bytes = 2*ITER*space_size*col_team_size*row_team_size;

  int max_threads = omp_get_max_threads();
  int first_level_omp_threads =  max_threads;
  int second_level_omp_threads = 1; 
  cs_exmpl_init_profile_info(max_threads);
  assert(space_size/chunk/first_level_omp_threads >0);
  ret=cspacer_get_channel_handle(0, channels+0); CHECK_CSPACE_RET(ret);
#if FP_REDUCTION
  cspacer_operation_t op = CSPACER_ADD_FP64;
#else
  cspacer_operation_t op = CSPACER_ADD_U64;
#endif
  ret = cspacer_construct_reduction (rmt_cspace_hndl,op,channels[0], tx_handles+0);  CHECK_CSPACE_RET(ret);

  cspacer_reduction_algorithm_t pattern = CSPACER_RED_SCATTER_BROADCAST_INORDER;
  ret = cspacer_plan_reduction(element_count,CSPACER_DEFAULT_REDUCTION_CHUNK,
          CSPACER_DEFAULT_FLUSH_DEPTH, pattern, INPLACE_REDUCTION, tx_handles[0]); CHECK_CSPACE_RET(ret);
  cspacer_declare_team_helper_threads(h_col_team, first_level_omp_threads);
  #pragma omp parallel num_threads(first_level_omp_threads)
  {
    int32_t nthreads = omp_get_num_threads();
    int32_t tid = omp_get_thread_num();
    cspacer_warm_space_threaded(src_cspace_hndl,tid,nthreads);
    cspacer_warm_space_threaded(rmt_cspace_hndl,tid,nthreads);
  }
  cspacer_barrier();

  gettimeofday(&start, NULL);

#if TIME_MARKER
  cs_exmpl_record_time_marker(0,0/*tid*/, start_cycle);
#endif

  for(iter = 0; iter<ITER; iter++) {
    start_cycle = read_cpu_cycles();

    #pragma omp parallel num_threads(first_level_omp_threads)
    {
      cspacer_return_t thr_ret;
      int32_t nthreads = omp_get_num_threads();
      int32_t tid = omp_get_thread_num();
      int32_t tx_count, t, iter;
      int64_t assignment, start,end, gap =0, thr_start_cycle, thr_end_cycle;
      cspacer_reduction_handle_t tx_hndl = tx_handles[0];
      tx_count = (element_count + chunk-1)/chunk;
  #if TIME_MARKER
      cs_exmpl_record_time_marker(1,tid, read_cpu_cycles());
  #endif
      cs_exmpl_record_thread_binding(0, tid);
      for(t=0;t<tx_count;t++) {
         /* 
          * We should be able to do computation with the full level of parallelism
          */
          start = t*chunk;
          end = (t+1)*chunk;
          if(end>element_count) end = element_count;
          assignment = end - start;
          if(assignment<0) continue;
          #if SPARSE_INPUT
          gap = chunk*t;
          #endif
          #pragma omp for nowait
          for(i=start;i<end;i++)
            src_space_ptr[i+gap] = rank+i+1;
          thr_start_cycle = read_cpu_cycles();
      #if COORDINATED_IMPLICIT_HANDLE
          thr_ret = cspacer_deposit_reduction_chunk_threaded(
             rmt_cspace_hndl, src_cspace_hndl,
              (start+gap), assignment, tid, nthreads, sync); CHECK_CSPACE_RET(thr_ret);
       #else
          thr_ret = cspacer_deposit_reduction_chunk_threaded(
            src_cspace_hndl, (start+gap), assignment, tid, nthreads, sync, tx_hndl); CHECK_CSPACE_RET(thr_ret);
       #endif
          
          thr_end_cycle = read_cpu_cycles();
          cs_exmpl_record_profile_info(t,tid, CS_INITIATE_TRANSFER, thr_end_cycle-thr_start_cycle);
        #if INJECTION_SPACER
          usleep(nap_time);
        #endif
      }
      /*We cannot free transfer before flushing the lane it is used for */
    //  thr_ret = cspacer_free_transfer(tx_hndl); CHECK_CSPACE_RET(thr_ret);
    }
    end_cycle = read_cpu_cycles();
    total_cycles += end_cycle - start_cycle;

    start_cycle = read_cpu_cycles();
    uint64_t tag = space_size;

    if(leader_sync) {
      ret = cspacer_get_node_leader_space_ptr(rmt_cspace_hndl, (void **)&dest_space_ptr); CHECK_CSPACE_RET(ret);
      leader =  cspacer_team_is_smp_leader(h_col_team) == CSPACER_SUCCESS;

  #if MULTI_THREADED_WAIT
      #pragma omp parallel num_threads(first_level_omp_threads)
      {
        cspacer_return_t thr_ret;
        int32_t nthreads = omp_get_num_threads();
        int32_t tid = omp_get_thread_num();
        thr_ret = cspacer_wait_leader_consistent_threaded(rmt_cspace_hndl,tag, tid, nthreads);  CHECK_CSPACE_RET(thr_ret);
      }
  #else
      cspacer_wait_leader_consistent(rmt_cspace_hndl,tag);  
  #endif
//    cspacer_team_smp_barrier(h_col_team);
    } else {
#if MULTI_THREADED_WAIT
      #pragma omp parallel num_threads(first_level_omp_threads)
      {
        cspacer_return_t thr_ret;
        int32_t nthreads = omp_get_num_threads();
        int32_t tid = omp_get_thread_num();
        thr_ret = cspacer_wait_consistent_threaded(rmt_cspace_hndl,tag, tid, nthreads); CHECK_CSPACE_RET(thr_ret);
      }
#else
      cspacer_wait_consistent(rmt_cspace_hndl,tag); 
#endif
    }
    end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(0,0, CS_WAIT_CONSISTENT,end_cycle-start_cycle);
    total_cycles += end_cycle - start_cycle;

    if(iter<ITER-1) {
      cspacer_barrier();
      cspacer_reset_state(rmt_cspace_hndl,CSPACER_RESET_INIT);
      ret = cspacer_plan_reduction(element_count,CSPACER_DEFAULT_REDUCTION_CHUNK,
          CSPACER_DEFAULT_FLUSH_DEPTH, pattern, INPLACE_REDUCTION, tx_handles[0]); CHECK_CSPACE_RET(ret);
      cspacer_barrier();
      if(iter == 0) {
        cs_exmpl_init_profile_info(max_threads);
        gettimeofday(&start, NULL);
        total_cycles = 0;
      }
    }
  }
  gettimeofday(&end, NULL);
//  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);
  cspacer_barrier();
  if(ITER>1) { exchanged_bytes = exchanged_bytes/ITER; ITER--; exchanged_bytes*= ITER;}

  time_ms = ((double) (end.tv_sec - start.tv_sec) * 1e3 + end.tv_usec/1e3) - (double)start.tv_usec / 1e3;

  total_time_ms = time_ms;
  cspacer_hw_reduction(CSPACER_ADD_FP64, &total_time_ms);
  time_ms = total_time_ms/univ_team_size;

  bw = exchanged_bytes/(time_ms*1e3);




  if(rank==PROFILE_RANK)
    fprintf(stderr,"Problem setup:\n"
        " Reduction across columns Maximum threads: %d\n"
        "    First level %d second level %d\n"
        "    Column Team: %d\n"
        " Element count: %d, chunk %d words, Iterations %d\n"
        " Rank count: %d\n"
        " Time: %f ms\n"
        " BW: %f MB/s\n",
        max_threads,
        first_level_omp_threads, second_level_omp_threads,
        col_team_size,
        element_count, chunk, ITER,
        univ_team_size,
        time_ms/ITER,
        bw);

  ret = cspacer_free_reduction(tx_handles[0]); CHECK_CSPACE_RET(ret);
    
  if(rank==PROFILE_RANK)
    fprintf(stderr,"Start Checking results:...\n");

#if VERIFY_SUBSET
  for(i=0;i<16;i++) {
    uint64_t expected = (col_team_size)*(2*row_rank+col_team_size*(row_team_size-1))/2+(i+1)*4;
    if(dest_space_ptr[i] != expected)
      fprintf(stderr,"Rank %d error at pos %d found %"PRIu64" expected %"PRIu64"\n", rank, i, dest_space_ptr[i], expected); 
  }

#else
#if !FP_REDUCTION
  #pragma omp parallel for private(i)
  for(i=0;i<element_count;i++) {
//    uint64_t expected = (col_team_size)*(2*row_rank+col_team_size*(row_team_size-1))/2+(i+1)*4;
    uint64_t expected = ((uint64_t)(col_team_size))*((uint64_t)i+ (row_rank+1) +row_team_size*(col_team_size-1)+(row_rank+1) + i )/2;
    if(dest_space_ptr[i] != expected) {
      fprintf(stderr,"Rank %d error at pos %d found %"PRIu64" expected %"PRIu64"\n", rank, i, dest_space_ptr[i], expected); 
      fflush(stderr);
    }
  }
  #endif
#endif
  if(rank==PROFILE_RANK) {
    fprintf(stderr,"finished checking\n");
    cs_exmpl_record_profile_global(0,end_cycle-start_cycle,total_cycles, ITER);
    cs_example_dump_profile_info(rank);
  }
  cspacer_fini_runtime();
  return 0;
}

