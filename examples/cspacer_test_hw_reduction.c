/*------------------------------------------------------------------------------
 * Description: CSPACER test pattern of Cray hardware reduction.
 * Author: Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/


#include "cspacer.h"
#include "cspacer_pattern_utils.h"
#include "cspacer_profile.h"
#include "assert.h"
#include "omp.h"

#include <unistd.h>
#include <sys/time.h>

#define FP_TEST               0
#define TEST_BARRIER          0

cs_profile_info_t cs_profile_info;

int main(int argc, char *argv[])
{
  int32_t lane_count, team_count,i,j, rank, partner;
  int32_t univ_team_size;
  uint64_t start_cycle, end_cycle,total_cycles, space_size, tr_start_cycle, tr_end_cycle;
  int nap_time = 4;
  struct timeval start, end;
  double time_us;
  uint64_t * src_space_ptr, *dest_space_ptr;
  int32_t ITER;

  char *config_file_name;
  cspacer_return_t ret;

  parse_options(argc,argv);
  ITER = cspacer_options.iteration_count;
  config_file_name = cspacer_options.config_file;

  ret = cspacer_init_runtime(argc,argv,config_file_name); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_lane_count(&lane_count); CHECK_CSPACE_RET(ret);
  cspacer_team_handle_t h_self_team, h_univ_team;
  ret = cspacer_get_team_handle(CSPACER_SELF_TEAM, &h_self_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_UNIVERSAL_TEAM, &h_univ_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_univ_team, &univ_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_univ_team, &rank); CHECK_CSPACE_RET(ret);
  int max_threads = omp_get_max_threads();
  int first_level_omp_threads = MIN(lane_count, max_threads);
  lane_count = first_level_omp_threads;
  cs_exmpl_init_profile_info(lane_count);

  cspacer_declare_team_helper_threads(h_univ_team, first_level_omp_threads);
#if FP_TEST
//  cspacer_operation_t op = CSPACER_ADD_FP64;
//  float value=1.0, result, expected;
  cspacer_operation_t op = CSPACER_MAX_FP64;
  double value=1.0*rank, result, expected;
#else
  cspacer_operation_t op = CSPACER_ADD_U32;
  uint32_t value, result, expected;
#endif
  cspacer_barrier();
  while((ret = cspacer_initiate_hw_reduction(op, &value)) != CSPACER_SUCCESS) 
      ;
  while(cspacer_check_hw_reduction(&result)!=CSPACER_SUCCESS)
      ;
  cspacer_barrier();
  gettimeofday(&start, NULL);
  start_cycle = read_cpu_cycles();
  for(i = 0;i<ITER;i++) {
#if TEST_BARRIER
    cspacer_barrier();
#else
    if(i==1) {
      cs_exmpl_init_profile_info(lane_count);
      gettimeofday(&start, NULL);
      start_cycle = read_cpu_cycles();
    }
    value = (rank+i);
    tr_start_cycle = read_cpu_cycles();
    while((ret = cspacer_initiate_hw_reduction(op, &value)) != CSPACER_SUCCESS) {
       tr_end_cycle = read_cpu_cycles();
       cs_exmpl_record_profile_info(i, 0, CS_ATTEMPT_TRANSFER, tr_end_cycle-tr_start_cycle);
       tr_start_cycle = tr_end_cycle;
    }
    tr_end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(i, 0, CS_ATTEMPT_TRANSFER, tr_end_cycle-tr_start_cycle);
    tr_start_cycle = read_cpu_cycles();
    while(cspacer_check_hw_reduction(&result)!=CSPACER_SUCCESS)
      ;
    tr_end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(i, 0, CS_WAIT_CONSISTENT, tr_end_cycle-tr_start_cycle);
    expected  = (double)(univ_team_size-1+2*i)*univ_team_size/2;
#if FP_TEST
    if(result != expected)
      printf ("Iter %d: result: %f, expected %f\n", i, result, expected); 
#else
    if(result != expected)
      printf ("Iter %d: result: %d, expected %d\n", i, result, expected); 
#endif
#endif
  }
  end_cycle = read_cpu_cycles();
  total_cycles = end_cycle - start_cycle;
  gettimeofday(&end, NULL);
  cspacer_barrier();

  time_us =  ((double) (end.tv_sec - start.tv_sec) * 1e3 + end.tv_usec/1e3) - (double)start.tv_usec / 1e3;
  time_us *= 1000;
  if(ITER>1) ITER--;
  time_us /= ITER;
  if(rank==PROFILE_RANK)
    printf("Problem setup:\n"
        " HW Collective Benchmark, maximum threads: %d\n"
        "    First level %d\n"
        " Iterations: %d\n"
        " Rank count: %d\n"
        " Time: %f us\n",
        max_threads,
        first_level_omp_threads,
        ITER,
        univ_team_size,
        time_us/ITER);
  if(rank==PROFILE_RANK) 
  {
    cs_exmpl_record_profile_global(0, end_cycle-start_cycle, total_cycles, ITER);
    cs_example_dump_profile_info(rank);
  }

  cspacer_fini_runtime();
  return 0;
}

