/*------------------------------------------------------------------------------
 * Description: Common header for pattern development using
 *               the Space Consistency Runtime.
 * Copyright (c) 2019 for Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/

#ifndef __CSPASER_PATTERN_UTIL__
#define __CSPASER_PATTERN_UTIL__

#include <getopt.h>


char * cspacer_ret_to_string (cspacer_return_t ret)
{
	switch(ret) {
		case CSPACER_NOT_SUPPORTED:
			return "Not supported";
			break;
		case CSPACER_RESOURCE_ERR:
			return "Resource Error";
			break;
		case CSPACER_INVALID_PARAM:
			return "Invalid parameter";
			break;
	}
	return "Unknown Error";
}


#define CHECK_CSPACE_RET(ret) \
		if(ret != CSPACER_SUCCESS) fprintf(stderr,"Error %s at line %d in file %s\n",cspacer_ret_to_string(ret), __LINE__, __FILE__);


#define MIN(x,y) (((x)<(y))? (x): (y))

typedef struct cspacer_options_s {
  int iterations;
  int chunk_size;
  int log_message_size;
  int iteration_count;
	int leader_sync;
  int argument;
  char *config_file;
} cspacer_options_t;


cspacer_options_t cspacer_options;

static int parse_options (int argc, char *argv[])
{
  static struct option long_options[] = {
    {"help",              no_argument,        0,  'h'},
    {"log-message-size",  required_argument,  0,  'l'},
    {"chunk-size",        required_argument,  0,  'c'},
    {"iteration-count",   required_argument,  0,  'i'},
    {"leader-sync",       required_argument,  0,  's'},
    {"config-file",       required_argument,  0,  'f'},
    {"argument",          required_argument,  0,  'a'}

  };  

  const char optstring[] = "a:f:i:c:l:s:h";
  char c;
  int option_index = 0;
  cspacer_options.log_message_size = 0;
  cspacer_options.chunk_size = 1024;
  cspacer_options.iteration_count = 1;
  cspacer_options.config_file = NULL;
	cspacer_options.leader_sync = 0;
  cspacer_options.argument = 0;
  
   while ((c = getopt_long(argc, argv, optstring, long_options, &option_index)) != -1) {
     switch(c) {
       case 'h':
         break;
       case 'l':
         cspacer_options.log_message_size = atoi(optarg);
         break;
       case 'c':
         cspacer_options.chunk_size = atoi(optarg);
         break;
       case 'i':
         cspacer_options.iteration_count = atoi(optarg);
         break;
       case 'f':
         cspacer_options.config_file = strdup(optarg);
         break;
       case 's':
         cspacer_options.leader_sync = atoi(optarg);
         break;
       case 'a':
         cspacer_options.argument = atoi(optarg);
         break;
 
     }
   }
   if(!cspacer_options.config_file)
      cspacer_options.config_file = strdup("cspacer_config.txt");
   return 0;
}

#endif

