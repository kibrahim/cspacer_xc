/*------------------------------------------------------------------------------
 * Description: CSPACER Implemention of a threaded put operation (incomplete).
 * Author: Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/


#include "cspacer.h"
#include "cspacer_pattern_utils.h"
#include "cspacer_profile.h"
#include "assert.h"
#include "omp.h"

#include <unistd.h>
#include <sys/time.h>

#define DEF_LOG_ELEMENT_COUNT 23

#define INJECTION_SPACER  0

cs_profile_info_t cs_profile_info;

int main(int argc, char *argv[])
{
  int32_t lane_count, team_count,i,j, rank, partner, chunk;
  int32_t univ_team_size;
  cspacer_space_handle_t src_cspace_hndl, rmt_cspace_hndl;
  int32_t element_count;
  uint64_t start_cycle, end_cycle,total_cycles, space_size;
  int nap_time = 4;
  struct timeval start, end;
  double exchanged_bytes, time_ms, bw;
  uint64_t * src_space_ptr, *dest_space_ptr;
  int32_t ITER;
  cspacer_transfer_handle_t *tx_handles;
  int64_t log_element_count;

  char *config_file_name;
  cspacer_return_t ret;

  parse_options(argc,argv);
  chunk = cspacer_options.chunk_size;
  log_element_count = cspacer_options.log_message_size?cspacer_options.log_message_size:DEF_LOG_ELEMENT_COUNT;
  ITER = cspacer_options.iteration_count;
  config_file_name = cspacer_options.config_file;
  element_count = 1 << log_element_count;
  space_size = element_count*sizeof(uint64_t);


  ret=cspacer_init_runtime(argc,argv,config_file_name); CHECK_CSPACE_RET(ret);
  ret=cspacer_get_lane_count(&lane_count); CHECK_CSPACE_RET(ret);
  cspacer_team_handle_t h_self_team, h_univ_team;
  ret = cspacer_get_team_handle(CSPACER_SELF_TEAM,  &h_self_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_UNIVERSAL_TEAM, &h_univ_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_univ_team, &univ_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_univ_team,&rank); CHECK_CSPACE_RET(ret);

  cspacer_lane_handle_t *lanes = malloc(sizeof(cspacer_lane_handle_t)*lane_count);

  tx_handles = malloc(sizeof(cspacer_transfer_handle_t)*lane_count);

//  printf("Rank %d: lane %d depth is: %d\n", rank, 0, cspaser_get_lane_depth(lanes[0]));
  
  
  partner = (rank+univ_team_size/2)%univ_team_size;

  ret = cspacer_sym_allocate_memory(h_self_team, space_size,CSPACER_DEFAULT_SPACE, &src_cspace_hndl); CHECK_CSPACE_RET(ret);
  ret = cspacer_sym_allocate_memory(h_univ_team, space_size,CSPACER_DEFAULT_SPACE, &rmt_cspace_hndl); CHECK_CSPACE_RET(ret);
//  ret = cspacer_reset_state(rmt_cspace_hndl,0); CHECK_CSPACE_RET(ret);


  ret = cspacer_get_space_ptr(src_cspace_hndl, (void **)&src_space_ptr); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(rmt_cspace_hndl, (void **)&dest_space_ptr); CHECK_CSPACE_RET(ret);

  exchanged_bytes = ITER*space_size* univ_team_size;

  #pragma omp parallel for
  for(i=0;i<element_count;i++)
    src_space_ptr[i] = (rank<<log_element_count) + (i+1);

  #pragma omp parallel for
  for(i=0;i<element_count;i++)
    dest_space_ptr[i] = 0;// (rank<<log_element_count) + (i+100);

  assert(space_size%chunk ==0);
  assert(space_size%(lane_count*chunk) ==0);
  int max_threads = omp_get_max_threads();
  int first_level_omp_threads = MIN(lane_count, max_threads);
  lane_count = first_level_omp_threads;
  cs_exmpl_init_profile_info(lane_count);
  int second_level_omp_threads = first_level_omp_threads<max_threads? max_threads/first_level_omp_threads:1;
  assert(space_size/chunk/first_level_omp_threads >0);

  for(i=0;i<lane_count;i++) {
    ret=cspacer_get_lane_handle(i, lanes+i); CHECK_CSPACE_RET(ret);
   //fprintf(stderr,"Rank %d: lane %d depth is: %d\n", rank, i, cspaser_get_lane_depth(lanes[i]));
    ret =  cspacer_construct_put(rmt_cspace_hndl, 0, partner, 
                          src_cspace_hndl, 0, chunk,
                          lanes[i], tx_handles+i);  CHECK_CSPACE_RET(ret);
  }

  cspacer_declare_team_helper_threads(h_univ_team, first_level_omp_threads);
  cspacer_barrier();

//  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
  gettimeofday(&start, NULL);
  start_cycle = read_cpu_cycles();
#if TIME_MARKER
  cs_exmpl_record_time_marker(0,0/*tid*/, start_cycle);
#endif
  #pragma omp parallel num_threads(first_level_omp_threads)
  {
     cspacer_return_t thr_ret;
     int32_t nthreads = omp_get_num_threads();
     int32_t tid = omp_get_thread_num();
     int32_t tx_count, t, iter;
     int64_t thr_assignment, thr_start, thr_start_cycle, thr_end_cycle;
     cspacer_transfer_handle_t tx_hndl = tx_handles[tid];
     cspacer_lane_handle_t mylane = lanes[tid];
     thr_assignment = space_size/nthreads;
     cspacer_transfer_handle_t tx_handle;
     tx_count = thr_assignment/chunk;
#if TIME_MARKER
     cs_exmpl_record_time_marker(1,tid, read_cpu_cycles());
#endif
     cs_exmpl_record_thread_binding(0, tid);

     for(iter = 0;iter<ITER;iter++) {
        thr_start = thr_assignment*tid;
        thr_start_cycle = read_cpu_cycles();
        thr_ret = cspacer_update_transfer(thr_start, thr_start, chunk, tx_hndl);  CHECK_CSPACE_RET(thr_ret);
        thr_end_cycle = read_cpu_cycles();
        cs_exmpl_record_profile_info(t,tid, CS_UPDATE_TRANSFER, thr_end_cycle-thr_start_cycle);
        for(t=0;t<tx_count;t++) {
         /* 
          We should be able to do computation with the full level of parallelism
         #pragma omp parallel num_threads(second_level_omp_threads)
         {
       
         }
        */
        thr_start_cycle = read_cpu_cycles();
        while((thr_ret = cspacer_initiate_transfer(tx_hndl)) != CSPACER_SUCCESS) {
          thr_end_cycle = read_cpu_cycles();
          cs_exmpl_record_profile_info(t,tid, CS_ATTEMPT_TRANSFER, thr_end_cycle-thr_start_cycle);
          thr_start_cycle = thr_end_cycle;// read_cpu_cycles();
        }
        thr_end_cycle = read_cpu_cycles();
        cs_exmpl_record_profile_info(t,tid, CS_INITIATE_TRANSFER, thr_end_cycle-thr_start_cycle);

        thr_start += chunk;
        thr_start_cycle = read_cpu_cycles();
        thr_ret = cspacer_update_transfer(thr_start, thr_start, chunk, tx_hndl);  CHECK_CSPACE_RET(thr_ret);
        thr_end_cycle = read_cpu_cycles();
        cs_exmpl_record_profile_info(t,tid, CS_UPDATE_TRANSFER, thr_end_cycle-thr_start_cycle);

      }
      /*We cannot free transfer before flushing the lane it is used for */
      thr_start_cycle = read_cpu_cycles();
      cspacer_flush_lane(mylane);
      thr_end_cycle = read_cpu_cycles();
      cs_exmpl_record_profile_info(0,tid, CS_FLUSH_LANE, thr_end_cycle-thr_start_cycle);
    }
    //  thr_ret = cspacer_free_transfer(tx_hndl); CHECK_CSPACE_RET(thr_ret);
#if TIME_MARKER
     cs_exmpl_record_time_marker(2,tid, read_cpu_cycles());
#endif
  }

  end_cycle = read_cpu_cycles();
  total_cycles = end_cycle - start_cycle;
  start_cycle = read_cpu_cycles();
#if TIME_MARKER
  cs_exmpl_record_time_marker(3,0,  start_cycle);
#endif

  uint64_t tag = space_size*ITER;
  while((ret = cspacer_is_consistent(rmt_cspace_hndl, tag)) != CSPACER_SUCCESS)
    ; // CHECK_CSPACE_RET(ret);
  end_cycle = read_cpu_cycles();

  cs_exmpl_record_profile_info(0,0, CS_WAIT_CONSISTENT,end_cycle-start_cycle);
#if TIME_MARKER
  cs_exmpl_record_time_marker(4,0,  end_cycle);
#endif


  total_cycles += end_cycle - start_cycle;
  gettimeofday(&end, NULL);

//  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);

  cspacer_barrier();

  time_ms = ((double) (end.tv_sec - start.tv_sec) * 1e3 + end.tv_usec/1e3) - (double)start.tv_usec / 1e3;
  bw = exchanged_bytes/(time_ms*1e3);



  if(rank==PROFILE_RANK)
    printf("Problem setup:\n"
        " Maximum threads: %d\n"
        "    First level %d second level %d\n"
        " Data size: %d, chunk %d bytes, Iterations: %d\n"
        " Rank count: %d\n"
        " Time: %f ms\n"
        " BW: %f MB/s\n",
        max_threads,
        first_level_omp_threads, second_level_omp_threads,
        space_size, chunk, ITER,
        univ_team_size,
        time_ms/ITER,
        bw
        );

//  sleep(10);  

    for(i=0;i<lane_count;i++) {
      ret = cspacer_free_transfer(tx_handles[i]); CHECK_CSPACE_RET(ret);
    }
//  printf("Rank %d: lane %d finish depth is: %d\n", rank, 0, cspaser_get_lane_depth(lanes[0]));
//  fflush(stderr);


  if(rank==PROFILE_RANK)
    printf("Start Checking results:...\n");
  #pragma omp parallel for
  for(i=0;i<element_count;i++) {
    uint64_t expected = (partner << log_element_count)+(i+1);
    if(dest_space_ptr[i] != expected)
      printf("Rank %d error at pos %d found %"PRIu64" expected %"PRIu64"\n", rank, i, dest_space_ptr[i], expected);
  }
  if(rank==PROFILE_RANK) 
  {
    printf("finished checking\n");
    cs_exmpl_record_profile_global(0, end_cycle-start_cycle, total_cycles, ITER);
    cs_example_dump_profile_info(rank);
  }
  cspacer_fini_runtime();
  return 0;
}

