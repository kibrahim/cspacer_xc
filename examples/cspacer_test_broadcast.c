/*------------------------------------------------------------------------------
 * Description: CSPACER test pattern of implementing a one-sided 
 *              broadcast pattern.
 * Author: Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/


#include "cspacer.h"
#include "cspacer_pattern_utils.h"
#include "cspacer_profile.h"
#include "assert.h"
#include "omp.h"

#include <unistd.h>
#include <sys/time.h>

#define DEF_LOG_ELEMENT_COUNT 23

#define INJECTION_SPACER  0
#define COL_SKEW          1
#define MULTI_THREADED_WAIT 1

cs_profile_info_t cs_profile_info;

int main(int argc, char *argv[])
{
  int32_t lane_count, team_count,i,j, col_rank,row_rank, rank, partner, chunk, root, leader;
  int32_t col_team_size, row_team_size, univ_team_size, leader_sync;
  cspacer_space_handle_t src_cspace_hndl, rmt_cspace_hndl;
  int32_t element_count;
  uint64_t start_cycle, end_cycle, total_cycles;
  uint64_t space_size;
  int nap_time = 1000;
  struct timeval start, end;
  double exchanged_bytes, time_ms, total_time_ms, bw;
  uint64_t * src_space_ptr, *dest_space_ptr;
  int32_t ITER;
  cspacer_broadcast_handle_t *tx_handles;
  cspacer_team_handle_t h_self_team, h_col_team, h_row_team, h_univ_team;
  int64_t log_element_count;

  char *config_file_name;
  cspacer_return_t ret;
  
  parse_options(argc,argv);
  chunk = cspacer_options.chunk_size;
  log_element_count = cspacer_options.log_message_size?cspacer_options.log_message_size:DEF_LOG_ELEMENT_COUNT;
  ITER = cspacer_options.iteration_count;
  config_file_name = cspacer_options.config_file;
  leader_sync = cspacer_options.leader_sync;

  ret=cspacer_init_runtime(argc,argv,config_file_name); CHECK_CSPACE_RET(ret);
  ret=cspacer_get_lane_count(&lane_count); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_SELF_TEAM,   &h_self_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_COLUMN_TEAM, &h_col_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_ROW_TEAM,    &h_row_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_UNIVERSAL_TEAM, &h_univ_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_col_team, &col_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_row_team, &row_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_univ_team, &univ_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_col_team,&col_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_row_team,&row_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_univ_team,&rank); CHECK_CSPACE_RET(ret);
//  fprintf(stderr,"Rank %d: r %d c %d\n", rank, row_rank,col_rank);
  element_count = 1 << log_element_count;
#if COL_SKEW
  element_count -= 1024*8*row_rank;
  if(element_count<0)
    element_count = 1024*8;
#endif
  space_size = element_count*sizeof(uint64_t);

  root = (col_rank == row_rank);// && (rank!=0);

  cspacer_lane_handle_t *lanes = malloc(sizeof(cspacer_lane_handle_t)*lane_count);

  tx_handles = malloc(sizeof(cspacer_broadcast_handle_t)*lane_count);

//  printf("Rank %d: lane %d depth is: %d\n", rank, 0, cspaser_get_lane_depth(lanes[0]));
  

  partner = row_rank;

  ret = cspacer_sym_allocate_memory(h_self_team, space_size,CSPACER_DEFAULT_SPACE, &src_cspace_hndl); CHECK_CSPACE_RET(ret);
  ret = cspacer_sym_allocate_memory(h_col_team, space_size,CSPACER_BROADCAST_SPACE, &rmt_cspace_hndl); CHECK_CSPACE_RET(ret);
//  ret = cspacer_reset_state(rmt_cspace_hndl,0); CHECK_CSPACE_RET(ret);

  ret = cspacer_get_space_ptr(src_cspace_hndl, (void **)&src_space_ptr); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(rmt_cspace_hndl, (void **)&dest_space_ptr); CHECK_CSPACE_RET(ret);

  exchanged_bytes = ITER*space_size*col_team_size*row_team_size;

  #pragma omp parallel for
  for(i=0;i<element_count;i++)
    src_space_ptr[i] = (col_rank<<log_element_count) + (i+1);

  #pragma omp parallel for
  for(i=0;i<element_count;i++)
    dest_space_ptr[i] = 0;


  assert(space_size%chunk ==0);
  assert(space_size%(lane_count*chunk) ==0);
  int max_threads = omp_get_max_threads();
  int first_level_omp_threads = MIN(lane_count, max_threads);
  lane_count = first_level_omp_threads;
  cs_exmpl_init_profile_info(lane_count);

  int second_level_omp_threads = first_level_omp_threads<max_threads? max_threads/first_level_omp_threads:1;
  assert(space_size/chunk/first_level_omp_threads >0);

  if(root) {
    /* root injectors*/
    for(i=0;i<lane_count;i++) {
      ret=cspacer_get_lane_handle(i, lanes+i); CHECK_CSPACE_RET(ret);
      ret =  cspacer_construct_broadcast(rmt_cspace_hndl, 0, 
                          src_cspace_hndl, 0, chunk,
                          lanes[i], tx_handles+i);  CHECK_CSPACE_RET(ret);
    }
  } 
  
  cspacer_declare_team_helper_threads(h_col_team, first_level_omp_threads);
  cspacer_barrier();

//  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
  gettimeofday(&start, NULL);
  start_cycle = read_cpu_cycles();
#if TIME_MARKER
  cs_exmpl_record_time_marker(0,0/*tid*/, start_cycle);
#endif

  if(root) {
    #pragma omp parallel num_threads(first_level_omp_threads)
    {
      cspacer_return_t thr_ret;
      int32_t nthreads = omp_get_num_threads();
      int32_t tid = omp_get_thread_num();
      int32_t tx_count, t, iter;
      int64_t thr_assignment, thr_start, thr_start_cycle, thr_end_cycle;
      cspacer_broadcast_handle_t tx_hndl = tx_handles[tid];
      cspacer_lane_handle_t mylane = lanes[tid];
      thr_assignment = space_size/nthreads;
      thr_start = thr_assignment*tid;
      tx_count = thr_assignment/chunk;
#if TIME_MARKER
     cs_exmpl_record_time_marker(1,tid, read_cpu_cycles());
#endif
     cs_exmpl_record_thread_binding(0, tid);


      thr_ret = cspacer_update_broadcast(thr_start, thr_start, chunk, tx_hndl);  CHECK_CSPACE_RET(thr_ret);
      for(iter = 0;iter<ITER;iter++) {
        for(t=0;t<tx_count;t++) {
         /* 
          We should be able to do computation with the full level of parallelism
         #pragma omp parallel num_threads(second_level_omp_threads)
         {
       
         }
        */
          thr_start_cycle = read_cpu_cycles();
          while((thr_ret = cspacer_initiate_broadcast(tx_hndl)) != CSPACER_SUCCESS){
            thr_end_cycle = read_cpu_cycles();
            cs_exmpl_record_profile_info(t,tid, CS_ATTEMPT_TRANSFER, thr_end_cycle-thr_start_cycle);
            thr_start_cycle = thr_end_cycle;//read_cpu_cycles();
          }
          thr_end_cycle = read_cpu_cycles();
          cs_exmpl_record_profile_info(t,tid, CS_INITIATE_TRANSFER, thr_end_cycle-thr_start_cycle);
        #if INJECTION_SPACER
          usleep(nap_time);
        #endif

          thr_start += chunk;
          thr_ret = cspacer_update_broadcast(thr_start, thr_start, chunk, tx_hndl);  CHECK_CSPACE_RET(thr_ret);
        }
        /*We cannot free transfer before flushing the lane it is used for */
        thr_start_cycle = read_cpu_cycles();
        cspacer_flush_lane(mylane);
        thr_end_cycle = read_cpu_cycles();
        cs_exmpl_record_profile_info(0,tid, CS_FLUSH_LANE, thr_end_cycle-thr_start_cycle);
      }
    //  thr_ret = cspacer_free_transfer(tx_hndl); CHECK_CSPACE_RET(thr_ret);
    }
  }
  end_cycle = read_cpu_cycles();
  total_cycles = end_cycle - start_cycle;
  start_cycle = read_cpu_cycles();
  uint64_t tag = space_size*ITER;
  if(leader_sync) {
    ret = cspacer_get_node_leader_space_ptr(rmt_cspace_hndl, (void **)&dest_space_ptr); CHECK_CSPACE_RET(ret);
    leader =  cspacer_team_is_smp_leader(h_col_team) == CSPACER_SUCCESS;
  #if MULTI_THREADED_WAIT
    #pragma omp parallel num_threads(first_level_omp_threads)
    {
      cspacer_return_t thr_ret;
      int32_t nthreads = omp_get_num_threads();
      int32_t tid = omp_get_thread_num();
      thr_ret = cspacer_wait_leader_consistent_threaded(rmt_cspace_hndl,tag, tid, nthreads); CHECK_CSPACE_RET(thr_ret); 
    }
  #else
    cspacer_wait_leader_consistent(rmt_cspace_hndl,tag);  
  #endif
  //  cspacer_team_smp_barrier(h_col_team);
  } else {
  #if MULTI_THREADED_WAIT
    #pragma omp parallel num_threads(first_level_omp_threads)
    {
      cspacer_return_t thr_ret;
      int32_t nthreads = omp_get_num_threads();
      int32_t tid = omp_get_thread_num();
      thr_ret = cspacer_wait_consistent_threaded(rmt_cspace_hndl,tag, tid, nthreads); CHECK_CSPACE_RET(thr_ret);
    }
  #else
    cspacer_wait_consistent(rmt_cspace_hndl,tag); 
  #endif
  }
  end_cycle = read_cpu_cycles();
  cs_exmpl_record_profile_info(0,0, CS_WAIT_CONSISTENT,end_cycle-start_cycle);
  gettimeofday(&end, NULL);
  total_cycles += end_cycle - start_cycle;
  cspacer_barrier();

  time_ms = ((double) (end.tv_sec - start.tv_sec) * 1e3 + end.tv_usec/1e3) - (double)start.tv_usec / 1e3;

  total_time_ms = time_ms;
  cspacer_hw_reduction(CSPACER_ADD_FP64, &total_time_ms);
  time_ms = total_time_ms/univ_team_size; /*Average across all ranks */

  bw = exchanged_bytes/(time_ms*1e3);



  if(rank==PROFILE_RANK)
    fprintf(stderr,"Problem setup:\n"
        " Broadcast to %d members within the column team\n"
        " Maximum threads: %d\n"
        "    First level %d second level %d\n"
        " Data size: %d, chunk %d bytes\n"
        " Rank count: %d\n"
        " Time: %f ms\n"
        " BW: %f MB/s\n",
        col_team_size,
        max_threads,
        first_level_omp_threads, second_level_omp_threads,
        space_size,chunk,
        univ_team_size,
        time_ms,
        bw
        );

  if(root) 
    for(i=0;i<lane_count;i++) {
      ret = cspacer_free_broadcast(tx_handles[i]); CHECK_CSPACE_RET(ret);
    }


  if(rank==PROFILE_RANK)
    fprintf(stderr,"Start Checking results:...\n");

  #pragma omp parallel for
  for(i=0;i<element_count;i++) {
    uint64_t expected = (partner << log_element_count)+(i+1);
    if(dest_space_ptr[i] != expected)
      fprintf(stderr,"Rank %d error at pos %d found %"PRIu64" expected %"PRIu64"\n", rank, i, dest_space_ptr[i], expected); 
  }
  if(rank==PROFILE_RANK) {
    fprintf(stderr,"finished checking\n");
    cs_exmpl_record_profile_global(0,end_cycle-start_cycle,total_cycles, ITER);
    cs_example_dump_profile_info(rank);
  }
  cspacer_fini_runtime();
  return 0;
}

