/*------------------------------------------------------------------------------
 * Description: CSPACER test configuration loader.
 * Copyright (c) 2019 for Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/

#include "cspacer.h"
#include "cspacer_util.h"

#include <stdio.h>
#define _GNU_SOURCE
#include <string.h>



int main(int argc, char *argv[])
{
  char * config_file_name = "./cspacer_config.txt";
  int32_t i, j, lane_count, team_count, rank_count, rank_id, self_rank, team_color;
  if(argc>1)
    rank_id = atoi(argv[1]);

  cspacer_config_file_handle_t cfh = cspaceri_read_config_file(config_file_name);


  cspaceri_read_key_value_int(cfh,CSPACER_RANK_COUNT_KEY,&rank_count, CSPACER_DEFAULT_RANK_COUNT);
  cspaceri_read_key_value_int(cfh,CSPACER_LANE_COUNT_KEY,&lane_count, CSPACER_DEFAULT_LANE_COUNT);
  cspaceri_read_key_value_int(cfh,CSPACER_TEAM_COUNT_KEY,&team_count, CSPACER_DEFAULT_TEAM_COUNT);


  int32_t * team_seg_memsize = (int32_t *) malloc(sizeof(int32_t) * team_count);

  int32_t * spaces_per_seg = (int32_t *) malloc(sizeof(int32_t) * team_count);
  int32_t * ranks_per_team = (int32_t *) malloc(sizeof(int32_t) * team_count);

  cspaceri_read_key_value_int_array(cfh,CSPACER_SEG_SIZE_MB_KEY, -1 /*sub_key*/,team_seg_memsize,team_count,CSPACER_DEFAULT_SEG_SIZE_MB);
  cspaceri_read_key_value_int_array(cfh,CSPACER_MAX_SPACES_PER_SEGMENT_KEY, -1 /*sub_key*/,spaces_per_seg,team_count,CSPACER_DEFAULT_SPACES_PER_SEGMENT);
  cspaceri_read_key_value_int_array(cfh,CSPACER_RANKS_PER_TEAM_KEY, -1 /*sub_key*/,ranks_per_team,team_count,CSPACER_DEFAULT_RANKS_PER_TEAM);
  for(i=0;i<team_count;i++) {
    int32_t *team_ranks = malloc(ranks_per_team[i]*sizeof(uint32_t));
    cspaceri_read_key_value_team_array(cfh, rank_id, i, team_ranks, &self_rank, &team_color);
    printf("Group %d: color %d self %d\n", i, team_color, self_rank);
    printf("ranks: ");
    for(j=0;j<ranks_per_team[i];j++)
      printf("%3d",team_ranks[j]);
    printf("\n");
  }
  return 0;
}

