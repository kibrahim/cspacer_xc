/*------------------------------------------------------------------------------
 * Description: Common profiling primitives for pattern development for 
 *               the Space Consistency Runtime.
 * Copyright (c) 2019 for Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/

#ifndef __CSPACER_EXAMPLE_PROFILE_H__
#define __CSPACER_EXAMPLE_PROFILE_H__

#include <time.h>
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <omp.h>

#define MAX_STAGE_COUNT 	1
#define MAX_MARKER_COUNT 	8

#define MAX_THREAD_COUNT 	256
#define PROFILE_RANK			(univ_team_size-1)
//#define PROFILE_RANK			(0)
#define CACHE_LINE_SIZE		64
#define ENABLE_PROFILE		1
#define TIME_MARKER				0


const static char * cs_profile_bin_str[] = {
	"Create Transfer Time",
	"Attempt Transfer Time",
	"Init Transfer Time",
	"Update Transfer Time",
	"Flush Lane Time",
	"Wait Metadata Time",
	"Wait Consistent Time",
  "Wait Compute Time"
};

typedef enum {
	CS_CREATE_TRANSFER = 0,
	CS_ATTEMPT_TRANSFER = 1,
	CS_INITIATE_TRANSFER = 2,
	CS_UPDATE_TRANSFER = 3,
	CS_FLUSH_LANE = 4,
	CS_WAIT_METADATA	= 5,
	CS_WAIT_CONSISTENT = 6,
	CS_WAIT_COMPUTE = 7,
	/* Should be the last */
	CS_PROFILE_BIN_COUNT = 8
} cs_profile_enum_t;

typedef struct __attribute__((aligned(CACHE_LINE_SIZE))) cs_profile_bin_s
{
	int64_t 	cycles[CS_PROFILE_BIN_COUNT];
	int64_t 	min_cycles[CS_PROFILE_BIN_COUNT];
	int64_t 	max_cycles[CS_PROFILE_BIN_COUNT];
	int64_t 	total_cycles;
	int64_t 	count;
	int64_t 	attempts;
	int32_t 	cpu_id;
} cs_profile_bin_t;

typedef struct cs_profile_info_s
{
	cs_profile_bin_t stage_stats[MAX_THREAD_COUNT][MAX_STAGE_COUNT];
#if TIME_MARKER
	int64_t 	time_marker[MAX_THREAD_COUNT][MAX_MARKER_COUNT];
	int 			recorded_marks;
#endif
	float 		init_time;
	float 		wait_time;
	float 		overall_time;
	int32_t 	stage_count;
	int32_t 	nthreads;
	int32_t 	iter;
	uint64_t 	core_freq;
} cs_profile_info_t;


extern cs_profile_info_t cs_profile_info;



#if !ENABLE_PROFILE

static inline uint64_t read_cpu_cycles(void){return 0;}
static inline uint64_t cs_exmpl_get_core_freq(void) {return 10000;}

static float time_of_cycles(uint64_t cycles)
{
return 0;
}

static inline void cs_exmpl_init_profile_info(int32_t nthreads)
{
}

static inline void cs_exmpl_record_profile_global(uint64_t init_cycles, uint64_t wait_cycles, uint64_t total_cycles)
{
}

static inline void cs_example_dump_profile_info(int32_t rank)
{
}

static inline void cs_exmpl_record_profile_info(int32_t stage, int32_t tid, cs_profile_enum_t profile_bin, uint64_t cycles)
{
}

static inline void cs_exmpl_record_time_marker(int32_t stage,int tid, int64_t time_marker)
{

}

#else
static inline uint64_t read_cpu_cycles(void)
{
#if defined(__i386__)
  uint64_t x;
  __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
  return x;
#elif  defined(__x86_64__)
  unsigned hi, lo; 
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
#else
  return 0;
#endif
}

/*
 * Assume no frequency scaling
 */
static inline uint64_t cs_exmpl_get_core_freq(void)
{
  uint64_t cycle1, cycle2;
  struct timespec time;
  time.tv_sec = 0;
  time.tv_nsec = 50000000;
  cycle1 = read_cpu_cycles();
  nanosleep(&time, NULL);
  cycle2 = read_cpu_cycles();
//	printf("cycles %lld\n", cycle2-cycle1);
  return (cycle2-cycle1)/((time.tv_sec*1000000000+ time.tv_nsec)/1000);
}



static inline int getcpu() {
    #ifdef SYS_getcpu
    int cpu, status;
    status = syscall(SYS_getcpu, &cpu, NULL, NULL);
    return (status == -1) ? status : cpu;
    #else
    return -1; // unavailable
    #endif
}



static float time_of_cycles(uint64_t cycles)
{
	float time = ((float)cycles)/cs_profile_info.core_freq;
	return time;
}



static inline void cs_exmpl_init_profile_info(int32_t nthreads)
{
	memset(&cs_profile_info,0,sizeof(cs_profile_info_t));
	cs_profile_info.core_freq = cs_exmpl_get_core_freq();
	cs_profile_info.nthreads = nthreads;
}

static inline void cs_exmpl_record_thread_binding(int stage, int tid)
{
	stage = stage%MAX_STAGE_COUNT;
	cs_profile_info.stage_stats[tid][stage].cpu_id = getcpu();
}


static inline void cs_exmpl_record_profile_info(int32_t stage, int32_t tid, cs_profile_enum_t profile_bin, uint64_t cycles)
{
	 stage = stage%MAX_STAGE_COUNT;
	 tid = tid%MAX_THREAD_COUNT;
//	 float time = time_of_cycles(cycles);
	 cs_profile_bin_t *stage_stats = & cs_profile_info.stage_stats[tid][stage];
//	 if(profile_bin == CS_ATTEMPT_TRANSFER)
	 stage_stats->total_cycles += cycles;
	 stage_stats->cycles[profile_bin] += cycles;
//	 assert(profile_bin<CS_PROFILE_BIN_COUNT);
	 if(stage_stats->max_cycles[profile_bin]==0) {
			stage_stats->min_cycles[profile_bin] = cycles;
			stage_stats->max_cycles[profile_bin] = cycles;
	 } else {
		 if(cycles<stage_stats->min_cycles[profile_bin])
				stage_stats->min_cycles[profile_bin] = cycles;
		 if(cycles>stage_stats->max_cycles[profile_bin])
				stage_stats->max_cycles[profile_bin] = cycles;
	 }
	 if(CS_INITIATE_TRANSFER == profile_bin)
		  stage_stats->count++;
	 else if (CS_ATTEMPT_TRANSFER == profile_bin)
		 stage_stats->attempts++;
	 if((tid ==0) && (cs_profile_info.stage_count<=stage))
			cs_profile_info.stage_count = stage+1;	 
}

#if TIME_MARKER
static inline void cs_exmpl_record_time_marker(int32_t stage, int tid, int64_t time_marker)
{
	 stage = stage%MAX_MARKER_COUNT;
	 cs_profile_info.time_marker[tid][stage] = time_marker;
	 if(tid == 0) cs_profile_info.recorded_marks = stage+1;
}
#endif

static inline void cs_exmpl_record_profile_global(uint64_t init_cycles, uint64_t wait_cycles, uint64_t total_cycles, int32_t iter)
{
    cs_profile_info.init_time = time_of_cycles(init_cycles);
    cs_profile_info.wait_time = time_of_cycles(wait_cycles); 
		cs_profile_info.overall_time = time_of_cycles(total_cycles);
		cs_profile_info.iter = iter; 
}  


static inline void cs_example_dump_profile_info(int32_t rank)
{
	int32_t i,j,t,b, len;
	char summary_string[2048];
//	printf("Exit nthreads: %d\n",cs_profile_info.nthreads);
	int32_t iter = cs_profile_info.iter;
	if(cs_profile_info.stage_count>0) {
	 	sprintf(summary_string,"Rank %d Init %8.2f Total wait %8.2f Total time %8.2f core freq %lld ", rank,
      									cs_profile_info.init_time, cs_profile_info.wait_time/iter, cs_profile_info.overall_time/iter, cs_profile_info.core_freq);
		fprintf(stderr,"### OVERALLL (time in us) %s\n",summary_string);

		len =0;
		for(t=0;t<cs_profile_info.nthreads;t++)
			 len+= sprintf(summary_string+len, "%4d ",cs_profile_info.stage_stats[t][0].cpu_id);
		fprintf(stderr,"### Rank %d binding: %s ####\n",rank,summary_string);

		for(i=0;i<cs_profile_info.stage_count;i++) {
			fprintf(stderr, "### Stage %d ####\n", i);
			for(b=0; b<CS_PROFILE_BIN_COUNT;b++) {
				len =0;
				for(t=0;t<cs_profile_info.nthreads;t++)
				 	len+= sprintf(summary_string+len, "%8.2f ", time_of_cycles(cs_profile_info.stage_stats[t][i].cycles[b])/iter);
				fprintf(stderr,"  %24s : %s\n",cs_profile_bin_str[b],summary_string);
				len =0;
				for(t=0;t<cs_profile_info.nthreads;t++)
				 	len+= sprintf(summary_string+len, "%6.4f ", time_of_cycles( cs_profile_info.stage_stats[t][i].min_cycles[b]));
			 	fprintf(stderr,"		Min %24s : %s\n",cs_profile_bin_str[b],summary_string);
		 		len =0;
			 	for(t=0;t<cs_profile_info.nthreads;t++)
				 	len+= sprintf(summary_string+len, "%6.4f ", time_of_cycles(cs_profile_info.stage_stats[t][i].max_cycles[b]));
			 	fprintf(stderr,"		Max %24s : %s\n",cs_profile_bin_str[b],summary_string);

			}
	 		len =0;
			for(t=0;t<cs_profile_info.nthreads;t++)
				 	len+=sprintf(summary_string+len, "%8d ", cs_profile_info.stage_stats[t][i].attempts/iter);
			fprintf(stderr,"  %24s : %s\n","Attempt Count",summary_string);
	 		len =0;
			for(t=0;t<cs_profile_info.nthreads;t++)
				 	len+=sprintf(summary_string+len, "%8d ", cs_profile_info.stage_stats[t][i].count/iter);
			fprintf(stderr,"  %24s : %s\n","Invoc Count",summary_string);
	 		len =0;
			for(t=0;t<cs_profile_info.nthreads;t++)
				 	len+=sprintf(summary_string+len, "%lld ", cs_profile_info.stage_stats[t][i].total_cycles/iter);
			fprintf(stderr,"  %24s : %s\n","Cycles",summary_string);
		}
	}
	#if TIME_MARKER
	fprintf(stderr, "### Time Markers in us ####\n");
	for(i=0;i<cs_profile_info.recorded_marks;i++) {
		len =0;
		for(t=0;t<cs_profile_info.nthreads;t++) { 
//			len+=sprintf(summary_string+len, "%8lld ", cs_profile_info.time_marker[t][i]);
			len+=sprintf(summary_string+len, "%6.4f ", time_of_cycles(cs_profile_info.time_marker[t][i]));
  
    }
		fprintf(stderr,"   Stage %d: %s\n",i,summary_string);
	}
	#endif
}
#endif

#endif
