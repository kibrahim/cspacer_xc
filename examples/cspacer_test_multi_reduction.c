/*------------------------------------------------------------------------------
 * Description: CSPACER Implemention of a reduction using multiple algorithms.
 *              Most of these reductions are latency optimized.
 * Author: Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/COPYRIGHT.TXT.
 *------------------------------------------------------------------------------*/


#include "cspacer.h"
#include "cspacer_pattern_utils.h"
#include "cspacer_profile.h"
#include "assert.h"
#include "omp.h"

#include <unistd.h>
#include <sys/time.h>

#define DEF_LOG_ELEMENT_COUNT 6
#define RESIDUAL_ELEMENT      0
#define INJECTION_SPACER      0
#define VERIFY_SUBSET         0
#define FP_REDUCTION          0
#define MULTI_REDUCTION_COUNT 4

cs_profile_info_t cs_profile_info;

int main(int argc, char *argv[])
{
  int32_t channel_count, team_count,i,j, col_rank,row_rank, rank, partner, chunk, leader;
  int32_t col_team_size, row_team_size, univ_team_size, leader_sync;
  cspacer_space_handle_t src_cspace_hndl, rmt_cspace_hndl[MULTI_REDUCTION_COUNT];
  int32_t element_count, algorithm, reset_space = -1, pre_reset_space = -1, space_idx;
  uint64_t start_cycle, end_cycle, total_cycles=0;
  cspacer_reset_type_t  reset_type;
  uint64_t space_size, tag;
  int nap_time = 1000, iter;
  struct timeval start, end;
  double exchanged_bytes, time_ms, total_time_ms, bw;
  uint64_t * src_space_ptr, *dest_space_ptr[MULTI_REDUCTION_COUNT];
  int32_t ITER, WARMUP;
  cspacer_reduction_handle_t tx_handles[MULTI_REDUCTION_COUNT];
  cspacer_channel_handle_t *channels;
  cspacer_team_handle_t h_self_team, h_col_team, h_row_team, h_univ_team;
  int64_t log_element_count;

  char *config_file_name;
  cspacer_return_t ret;
  cspacer_sync_mode_t sync;

  parse_options(argc,argv);
  chunk = cspacer_options.chunk_size;
  log_element_count = cspacer_options.log_message_size?cspacer_options.log_message_size:DEF_LOG_ELEMENT_COUNT;
  ITER = cspacer_options.iteration_count;
  config_file_name = cspacer_options.config_file;
  leader_sync = cspacer_options.leader_sync;
  algorithm = cspacer_options.argument;


  ret = cspacer_init_runtime(argc,argv,config_file_name); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_channel_count(&channel_count); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_SELF_TEAM, &h_self_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_COLUMN_TEAM, &h_col_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_ROW_TEAM, &h_row_team); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_team_handle(CSPACER_UNIVERSAL_TEAM, &h_univ_team); CHECK_CSPACE_RET(ret);

  ret = cspacer_team_size(h_row_team, &row_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_col_team, &col_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_size(h_univ_team, &univ_team_size); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_col_team, &col_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_row_team, &row_rank); CHECK_CSPACE_RET(ret);
  ret = cspacer_team_rank(h_univ_team, &rank); CHECK_CSPACE_RET(ret);

  element_count = (1 << log_element_count);
  space_size = element_count*sizeof(uint64_t);
  

  channels = malloc(sizeof(cspacer_channel_handle_t)*channel_count);

  for(i=0;i<MULTI_REDUCTION_COUNT;i++) {
    ret = cspacer_sym_allocate_memory(h_col_team, space_size,CSPACER_REDUCTION64_SPACE, rmt_cspace_hndl+i); CHECK_CSPACE_RET(ret);
    ret = cspacer_get_space_ptr(rmt_cspace_hndl[i], (void **)&(dest_space_ptr[i])); CHECK_CSPACE_RET(ret);
  }

  ret = cspacer_sym_allocate_memory(h_self_team, 2*space_size,CSPACER_DEFAULT_SPACE, &src_cspace_hndl); CHECK_CSPACE_RET(ret);
  ret = cspacer_get_space_ptr(src_cspace_hndl, (void **)&src_space_ptr); CHECK_CSPACE_RET(ret);
  sync = CSPACER_ALL_SYNC;

  for(j=0;j<MULTI_REDUCTION_COUNT;j++) {
    for(i=0;i<element_count;i++)
      dest_space_ptr[j][i] = 0; 
  }
  for(i=0;i<element_count;i++)
    src_space_ptr[i] = rank+i+1;

  exchanged_bytes = 2*ITER*space_size*col_team_size*row_team_size;

  int max_threads = 1; 
  cs_exmpl_init_profile_info(max_threads);
  ret=cspacer_get_channel_handle(0, channels+0); CHECK_CSPACE_RET(ret);
#if FP_REDUCTION
  cspacer_operation_t op = CSPACER_ADD_FP64;
#else
  cspacer_operation_t op = CSPACER_ADD_U64;
#endif
  cspacer_declare_team_helper_threads(h_col_team, 1/*first_level_omp_threads*/);
  cspacer_reduction_algorithm_t pattern;
  switch(algorithm) {
    case 0:
    default:
      pattern = CSPACER_RED_DEFAULT;
      break;
    case 1:
      pattern = CSPACER_RED_RECURSIVE_PRIME_CUBES_2;
      break;
    case 2:
      pattern = CSPACER_RED_RECURSIVE_PRIME_CUBES_4;
      break;
    case 3:
      pattern = CSPACER_RED_RECURSIVE_PRIME_CUBES_8;
      break;
  }

  for(j=0;j<MULTI_REDUCTION_COUNT;j++) {
    ret = cspacer_construct_reduction (rmt_cspace_hndl[j], op, channels[0], tx_handles+j);  CHECK_CSPACE_RET(ret);
    ret = cspacer_plan_reduction(element_count,CSPACER_DEFAULT_REDUCTION_CHUNK,
          CSPACER_DEFAULT_FLUSH_DEPTH, pattern, 0/*INPLACE_REDUCTION*/, tx_handles[j]); CHECK_CSPACE_RET(ret);
  }
  cspacer_warm_space_threaded(src_cspace_hndl,0/*tid*/,1/*nthreads*/);
  for(i=0;i<MULTI_REDUCTION_COUNT;i++)
    cspacer_warm_space_threaded(rmt_cspace_hndl[i],0/*tid*/,1/*nthreads*/);
  leader =  cspacer_team_is_smp_leader(h_col_team) == CSPACER_SUCCESS;
  tag = space_size;
  if(leader_sync) {
    reset_type = CSPACER_RESET_PEER_NOSYNC;
    ret = cspacer_get_node_leader_space_ptr(rmt_cspace_hndl[0], (void **)&dest_space_ptr[0]); CHECK_CSPACE_RET(ret);
  } else
    reset_type = CSPACER_RESET_PEER_SYNC;
  WARMUP = MULTI_REDUCTION_COUNT*2;

  cspacer_barrier();
  gettimeofday(&start, NULL);
#if TIME_MARKER
  cs_exmpl_record_time_marker(0,0, start_cycle);
#endif
  for(iter = 0; iter<ITER+WARMUP; iter++) {
    start_cycle = read_cpu_cycles();
    i = iter%MULTI_REDUCTION_COUNT;
    cspacer_reduction_handle_t tx_hndl = tx_handles[i];
    #if COORDINATED_IMPLICIT_HANDLE
    ret = cspacer_deposit_reduction_chunk(
             rmt_cspace_hndl[i], src_cspace_hndl,
             0, element_count); CHECK_CSPACE_RET(ret);
    #else
    ret = cspacer_deposit_reduction_chunk(
            src_cspace_hndl, 0, element_count, tx_hndl); CHECK_CSPACE_RET(ret);
    #endif
    end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(iter, 0 /*tid*/, CS_INITIATE_TRANSFER, end_cycle-start_cycle);
    total_cycles += end_cycle - start_cycle;
    start_cycle = read_cpu_cycles();

    if(leader_sync) {
      cspacer_wait_leader_consistent(rmt_cspace_hndl[i],tag);  
    } else 
      cspacer_wait_consistent(rmt_cspace_hndl[i],tag);
    
    end_cycle = read_cpu_cycles();
    cs_exmpl_record_profile_info(0,0, CS_WAIT_CONSISTENT,end_cycle-start_cycle);
    total_cycles += end_cycle - start_cycle;
    if(reset_space>=0) {
      if(leader_sync) 
        space_idx = reset_space;
      else {
         if(leader) 
           space_idx = reset_space;
         else 
           space_idx = pre_reset_space;
      }
      if(space_idx>=0) {
         cspacer_reset_state(rmt_cspace_hndl[space_idx], reset_type);
         ret = cspacer_plan_reduction(element_count,CSPACER_DEFAULT_REDUCTION_CHUNK,
          CSPACER_DEFAULT_FLUSH_DEPTH, pattern, 0/*INPLACE_REDUCTION*/, tx_handles[space_idx]); CHECK_CSPACE_RET(ret);
      }
    }
    pre_reset_space = reset_space;
    reset_space = i;
    if(iter == WARMUP-1) {
      cs_exmpl_init_profile_info(max_threads);
      gettimeofday(&start, NULL);
      total_cycles = 0;
    }
  }
  gettimeofday(&end, NULL);

  cspacer_barrier();

  time_ms = ((double) (end.tv_sec - start.tv_sec) * 1e3 + end.tv_usec/1e3) - (double)start.tv_usec / 1e3;

  total_time_ms = time_ms;
  cspacer_hw_reduction(CSPACER_ADD_FP64, &total_time_ms);
  time_ms = total_time_ms/univ_team_size;

  bw = exchanged_bytes/(time_ms*1e3);

  if(rank==PROFILE_RANK)
    fprintf(stderr,"Problem setup:\n"
        " Reduction across columns Maximum threads: %d\n"
        "    Algorithm: %d\n"
        "    Column Team: %d\n"
        " Element count: %d, chunk %d words, Iterations %d\n"
        " Rank count: %d\n"
        " Time: %f us\n"
        " BW: %f MB/s\n",
        max_threads,
        algorithm,
        col_team_size,
        element_count, chunk, ITER,
        univ_team_size,
        1000*time_ms/ITER,
        bw);

  ret = cspacer_free_reduction(tx_handles[0]); CHECK_CSPACE_RET(ret);
    
  if(rank==PROFILE_RANK)
    fprintf(stderr,"Start Checking results:...\n");

#if !FP_REDUCTION
  for(i=0;i<element_count;i++) {
    uint64_t expected = ((uint64_t)(col_team_size))*((uint64_t)i+ (row_rank+1) +row_team_size*(col_team_size-1)+(row_rank+1) + i )/2;
    if(dest_space_ptr[0][i] != expected) {
      fprintf(stderr,"Rank %d error at pos %d found %"PRIu64" expected %"PRIu64"\n", rank, i, dest_space_ptr[0][i], expected); 
      fflush(stderr);
    }
  }
#endif
  if(rank==PROFILE_RANK) {
    fprintf(stderr,"finished checking\n");
    cs_exmpl_record_profile_global(0,end_cycle-start_cycle,total_cycles, ITER);
    cs_example_dump_profile_info(rank);
  }
  cspacer_fini_runtime();
  return 0;
}

