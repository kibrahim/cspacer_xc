# Consistent SPACE Runtime (CSPACER)


## License/Copyright

Please refer to:

 - doc/COPYRIGHT.TXT  

## Author

Khaled Ibrahim: Lawrence Berkeley National Laboratory

Contact: kzibrahim <AT> lbl.gov


## Introduction

This software provides a runtime for communication-bound applications, 
especially with irregular communication patterns. The target programming abstraction for 
the runtime is the space consistency model, which defines consistency guarantees 
at the granularity of memory spaces. This model has relaxed consistency semantics 
that enables a wide range of runtime optimizations. The runtime leverages threading 
to accelerate communication primitives, especially collective operations. It also allows 
efficient pipelining of communication operations and enable constructing a consistent state of 
multiple unordered communication activities targeting a memory space. 

The runtime uses a reduced API design that decomposes complex communication primitives 
in traditional general-purpose runtime into a sequence of simpler steps.

To improve the productivity of using this runtime, we provide communication patterns commonly used 
for regular scientific computing applications and irregular data analytics. These communication patterns 
offer skeletons for the integration with application computation to allow efficient overlap.
       

## Requirements

The package depends on Cray GNI headers (cray-gni-headers), which can be acquired from
crayport.cray.com. See detailed instruction in the directory "external." 


## Directory Structure

The CSPACER directory structure is as follows:

	include: Application programming headers
	src: Implementation of CSPACER core APIs
  lib: Prebuilt cspacer library. 
	examples: Various communication patterns implemented using CSPACER
            Currently, we implement
            - remote put
            - allreduce (pipelined injection)
            - broadcast (one-sided and coordinated)
            - allgather (one-sided and coordinated)
            - alltoall (including irregular)
            (all operations are implemented through single and multi-threaded operations)
	util: Various utilities for instance to generate CSPACER configuration files
	external: Cray headers (could be obtained from crayport.cray.com)
	doc: license documents


## Pre-Built instructions

In the lib directory, you will find a prebuilt library for the Cray XC40 systems. 
The library is built for NERSC Cori system (https://docs.nersc.gov/systems/cori/).
To request prebuilt libraries for other systems, please contact authors.


## Build


To build on Cray XC systems with GNU compiler.

```shell

$ module unload PrgEnv-intel
$ module load PrgEnv-gnu ugni
$ export CC=gcc

$ libtoolize
$ aclocal
$ automake --add-missing
$ autoconf

$ mkdir build
$ cd build

$ ../configure --prefix=<inst dir>

$ make
$ make install

```

