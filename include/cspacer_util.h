
#ifndef __CSPACER_UTIL__
#define __CSPACER_UTIL__

#include "gni_pub.h"
#include "inttypes.h"
#include <stdlib.h>
#include <stddef.h>


char *gni_return_string(gni_return_t status);

typedef struct cspacer_config_file_s
{
	char *config_file_buffer;
	uint64_t buffer_size;
} cspacer_config_file_t;

typedef struct cspacer_config_file_s * cspacer_config_file_handle_t;


cspacer_config_file_handle_t  cspaceri_read_config_file(char *config_file_name);
void cspaceri_free_config_file(cspacer_config_file_handle_t config_file_handle);
int32_t cspaceri_parse_key_value_double (cspacer_config_file_handle_t cfh,
		                  char *key, double *value, double default_val);

int32_t cspaceri_read_key_value_int_array (cspacer_config_file_handle_t cfh,
		                  char *key, int32_t  sub_key,  int32_t *value_arr,  int32_t array_size, int32_t default_val);

int32_t  cspaceri_read_key_value_int (cspacer_config_file_handle_t cfh,
		                   char *key, int32_t * value, int32_t default_val);

int32_t cspaceri_read_key_value_team_array(cspacer_config_file_handle_t cfh, int  rank_id, int32_t team_id, int32_t * team_ranks, int32_t * in_team_rank, int32_t * team_color);

int64_t  cspaceri_calc_distributed_assignment(int32_t tid, int32_t nthreads, int64_t count, 
		                int64_t *assignment_start, int64_t *assignment_end);


int32_t cspaceri_bsearch32(uint32_t key, const uint32_t *base, int32_t num);
int cspaceri_fuse_dims(char dims[32], int dim_count, int max_dim_count);

int cspaceri_factorize(uint64_t key, char default_factor, char factor_vector[32]);
int cspaceri_fuse_dims(char dims[32], int dim_count, int max_dim_count);
void cspaceri_get_coordinates(uint32_t rank, int dim_count, char dims[32], char coord[32]);
int cspaceri_get_rank(int dim_count, char dims[32], char coord[32]);

uint64_t cspaceri_read_cpu_cycles(void);

int32_t cspaceri_get_env_val(char * key, int idx);


#define CSPACER_ERR_FATAL		(1)
#define CSPACER_ERR_WARN 		(2)

#define CSPACER_ERROR(type, message, args...)  {                           				\
	    if ((type) & CSPACER_ERR_FATAL) fprintf(stderr, "CSpacer Fatal Error: "); 	\
	    if ((type) & CSPACER_ERR_WARN)  fprintf(stderr, "CSpacer Warning: ");      	\
	    fprintf(stderr, message, ##args);                                  					\
	    fprintf(stderr," at line %d in file %s\n", __LINE__, __FILE__);   					\
	    if (type == CSPACER_ERR_FATAL) abort();                               			\
	    }

char *gni_return_string(gni_return_t status);




#define CSPACER_GNI_CHECK(status) {     \
  if(status != GNI_RC_SUCCESS)  				\
    fprintf(stderr,"GNI Error %s at line %d in file %s\n",gni_return_string(status), __LINE__, __FILE__);\
  }

void cspacer_abort(void);
void cspaceri_clflush(void *ptr);

#endif

