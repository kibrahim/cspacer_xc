/*------------------------------------------------------------------------------
 * Description: Private header for the Space Consistency Runtime.
 * Copyright (c) 2019 for Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/license.txt.
 *------------------------------------------------------------------------------*/
#ifndef __CSPACER_PUB__
#define __CSPACER_PUB__

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef _GNU_SOURCE
#define _GNU_SOURCE 
#endif
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <assert.h>
#include <malloc.h>
#include <sched.h>
#include <sys/utsname.h>
#include <errno.h>

#define COORDINATED_IMPLICIT_HANDLE  1

/*! Reduced API for memory transfer
 * Each transfer life 
 *    construct
 *    initiate
 *    update
 *    initiate
 *    free
 */

typedef enum  cspacer_error_u
{
  CSPACER_SUCCESS,
  CSPACER_GENERAL_ERROR,
  CSPACER_NOT_SUPPORTED,
  CSPACER_INVALID_STATE,
  CSPACER_RESOURCE_ERR,
  CSPACER_INVALID_PARAM,
  CSPACER_INCONSISTENT_STATE,
  CSPACER_NOT_READY
} cspacer_return_t;

typedef enum  cspacer_sync_mode_u
{
  CSPACER_NO_SYNC=0,
  CSPACER_PRE_SYNC=1,
  CSPACER_POST_SYNC=2,
  CSPACER_ALL_SYNC=3
} cspacer_sync_mode_t;

typedef enum  cspacer_reset_type_u
{
  CSPACER_RESET_INIT=-1,
  CSPACER_RESET_PEER_NOSYNC=0,
  CSPACER_RESET_PEER_SYNC=1
} cspacer_reset_type_t;


typedef enum cspacer_operation_u
{
  CSPACER_OR_U32,
  CSPACER_XOR_U32,
  CSPACER_AND_U32,
  CSPACER_ADD_U32,
  CSPACER_ADD_FP32,
  CSPACER_SUB_FP32,
  CSPACER_MUL_FP32,
  CSPACER_MIN_FP32,
  CSPACER_MAX_FP32,

  CSPACER_OR_U64,
  CSPACER_XOR_U64,
  CSPACER_AND_U64,
  CSPACER_ADD_U64,
  CSPACER_ADD_FP64,
  CSPACER_SUB_FP64,
  CSPACER_MUL_FP64,
  CSPACER_MIN_FP64,
  CSPACER_MAX_FP64  
} cspacer_operation_t;



#define CSPACER_RANK_COUNT_KEY  "RANK COUNT"
#define CSPACER_DEFAULT_RANK_COUNT 4

#define CSPACER_LANE_COUNT_KEY "LANE COUNT"
#define CSPACER_DEFAULT_LANE_COUNT 64

#define CSPACER_REQ_PER_LANE_KEY "REQUEST PER LANE"
#define CSPACER_DEFAULT_MAX_REQ_COUNT     512

#define CSPACER_TEAM_COUNT_KEY  "MAX TEAMS PER RANK"
#define CSPACER_DEFAULT_TEAM_COUNT  4
#define CSPACER_LANES_PER_CHANNEL_KEY "LANES PER CHANNEL"
#define CSPACER_DEFAULT_LANES_PER_CHANNEL 4

#define CSPACER_MAX_HELPER_THREADS_KEY "MAX HELPER THREADS"
#define CSPACER_DEFAULT_MAX_HELPER_THREADS  64

#define CSPACER_DEFAULT_REDUCTION_CHUNK     (-1)

  /*  Each team has one attached segment.
   *  A segment may contain multiple spaces.
   */

#define CSPACER_RANKS_PER_TEAM_KEY "RANKS PER TEAM"
#define CSPACER_DEFAULT_RANKS_PER_TEAM 1
#define CSPACER_SEG_SIZE_MB_KEY  "SEGMENT PER TEAM"
#define CSPACER_DEFAULT_SEG_SIZE_MB (1024)
#define CSPACER_SEGMENT_MB (1024*1024)

#define CSPACER_SPACES_PER_SEG_KEY  "SPACES PER SEGMENT"
#define CSPACER_DEFAULT_SPACES_PER_SEG  (16)

#define CSPACER_MAX_SPACES_PER_SEGMENT_KEY "MAX SPACES PER SEGMENT"
#define CSPACER_DEFAULT_SPACES_PER_SEGMENT    32
#define CSPACER_INFO_ENV    "CSPACER_DUMP_INFO"

#define USE_ALL_LANES     (-1) // use all available lanes to process transactions

#define CSPACER_DEFAULT_FLUSH_DEPTH	(-1)



/*
 * This routine should be called before MPI_Init.
 * The symmetric allocation relies on initialization in a uniform may which may be 
 * impacted, if MPI_Init is called first.
 */
cspacer_return_t
  cspacer_init_runtime(
    int argc,
    char *argv[],
    char * config_file_name);

cspacer_return_t
  cspacer_fini_runtime(void);

typedef struct cspacer_space_s *cspacer_space_handle_t;
typedef struct cspacer_transfer_s *cspacer_transfer_handle_t;
typedef struct cspacer_team_s *cspacer_team_handle_t;
typedef struct cspacer_lane_s *cspacer_lane_handle_t;
typedef struct cspacer_channel_s *cspacer_channel_handle_t;
typedef struct cspacer_broadcast_s *cspacer_broadcast_handle_t;
typedef struct cspacer_coordinated_broadcast_s *cspacer_coordinated_broadcast_handle_t;
typedef struct cspacer_reduction_s *cspacer_reduction_handle_t;

/*
 *   An accumulate operation
 */

cspacer_return_t
  cspacer_construct_atomic_put(
    cspacer_space_handle_t dest_space,    /* In:  Dest Symmetric Space Handle */
    uint64_t dest_offset,                 /* In:  Aligned src offset */
    uint32_t target_rank,                 /* In:  rank within the dest team */
    cspacer_operation_t op,               /* In:  operation associated with atomic update */
    cspacer_space_handle_t src_space,     /* In:  Src Symmetric Space Handle */
    uint64_t  src_offset,                 /* In:  Offset at the source */
    uint64_t count,                       /* In:  A transfer size = count * sizeof(op_type). */
    cspacer_lane_handle_t injection_lane, /* In:  A lock free injection lane */
    cspacer_transfer_handle_t *handle);    /* Out: handle to operation */

/*
 *  An override put, which is typically the most efficient operation 
 *  by most interconnects.
 */

cspacer_return_t
  cspacer_construct_put(
    cspacer_space_handle_t dest_space,    /* In:  Dest Symmetric Space Handle */
    uint64_t dest_offset,                 /* In:  Aligned dest offset */
    uint32_t target_rank,                 /* In:  rank within the dest team */
    cspacer_space_handle_t src_space,     /* In:  Src Symmetric Space Handle */
    uint64_t  src_offset,                 /* In:  Offset at the source */
    uint64_t size,                        /* In:  A transfer size. Should satisfy alignment. */
    cspacer_lane_handle_t injection_lane, /* In:  A lock free injection lane */
    cspacer_transfer_handle_t *handle);    /* Out: handle to operation */

typedef  enum cspacer_coordinated_broadcast_algorithm_s {
  CSPACER_BROADCAST_BINARY_TREE,
  CSPACER_BROADCAST_INVALID
} cspacer_coordinated_broadcast_algorithm_t;



cspacer_return_t
  cspacer_construct_broadcast(
    cspacer_space_handle_t dest_space,    /* In:  Dest symmetric space handle */
    uint64_t dest_offset,                 /* In:  Aligned src offset */
    cspacer_space_handle_t src_space,     /* In:  Src Symmetric Space Handle */
    uint64_t  src_offset,                 /* In:  Offset at the source */
    uint64_t size,                        /* In:  A transfer size. Should satisfy alignment. */
    cspacer_lane_handle_t injection_lane, /* In:  A lock free injection lane */
    cspacer_broadcast_handle_t *handle);  /* Out: handle to memory */

  /*
   * A coordinated broadcast assume all processes are involved in 
   * progressing the spaces to read the data.
   * We have at most one active coordinated broadcast per space. 
   *
   */

cspacer_return_t
  cspacer_construct_coordinated_broadcast(
    cspacer_space_handle_t dest_space,    /* In:  Dest symmetric space handle. */
    cspacer_channel_handle_t broadcast_channel,
    cspacer_coordinated_broadcast_handle_t *ptx_hndl);    /* Out: handle to memory */

/*
 * Planning declare the root, and registerthe handle with the dest space. 
 * 
 */
cspacer_return_t
    cspacer_plan_coordinated_broadcast(
          int32_t root,                  /* In: The root of the coordinated broadcast. */
          int32_t flush_depth,           /* In: The depth of flushing requests */
          int32_t inplace,                      /* In: Is the broadcast in place or out of place. */
          cspacer_coordinated_broadcast_algorithm_t pattern,/* In: the algorithm to use for broadcast. */
          cspacer_coordinated_broadcast_handle_t tx_hndl);  /* Inout: The broadcast handle.  */

cspacer_return_t
     cspacer_activate_coordinated_broadcast(
           cspacer_coordinated_broadcast_handle_t tx_hndl);

#if COORDINATED_IMPLICIT_HANDLE
/*
 * The transfer handle in implicit in the choice of the dest space.
 * Only the root should call this routine. 
 */

cspacer_return_t
    cspacer_deposit_coordinated_broadcast_chunk_threaded(
      cspacer_space_handle_t dest_space,
      cspacer_space_handle_t src_space,     /* In:  Src Symmetric Space Handle. */
      uint64_t src_offset,                  /* In: Offset at the source. */
      uint64_t size,                        /* In: A transfer size. Should satisfy alignment. */
      int32_t tid,
      int32_t nthreads,
      cspacer_sync_mode_t sync
      );

/*
 * The transfer handle in implicit in the choice of the dest space.
 * Only the root should call this routine. 
 */

cspacer_return_t
    cspacer_deposit_coordinated_broadcast_chunk(
      cspacer_space_handle_t dest_space, 
      cspacer_space_handle_t src_space,     /* In:  Src Symmetric Space Handle. */
      uint64_t src_offset,                  /* In: Offset at the source. */
      uint64_t size);                        /* In: A transfer size. Should satisfy alignment. */
#else


cspacer_return_t
    cspacer_deposit_coordinated_broadcast_chunk_threaded(
      cspacer_space_handle_t src_space,     /* In:  Src Symmetric Space Handle. */
      uint64_t src_offset,                  /* In: Offset at the source. */
      uint64_t size,                        /* In: A transfer size. Should satisfy alignment. */
      int32_t tid,
      int32_t nthreads,
       cspacer_sync_mode_t sync,
      cspacer_coordinated_broadcast_handle_t tx_hndl);

cspacer_return_t
    cspacer_deposit_coordinated_broadcast_chunk(
      cspacer_space_handle_t dest_space,    /* In: Dest symmetric Space Handle. */
      cspacer_space_handle_t src_space,     /* In: Src Symmetric Space Handle. */
      uint64_t src_offset,                  /* In: Offset at the source. */
      uint64_t size,                        /* In: A transfer size. Should satisfy alignment. */
      cspacer_coordinated_broadcast_handle_t tx_hndl);

#endif


typedef  enum cspacer_reduction_algorithm_s {
  CSPACER_RED_SCATTER_BROADCAST_INORDER=0,
  CSPACER_RED_RECURSIVE_PRIME_CUBES_2=1,
  CSPACER_RED_RECURSIVE_PRIME_CUBES_4=2,
  CSPACER_RED_RECURSIVE_PRIME_CUBES_8=3,
  CSPACER_RED_DEFAULT=4,
  CSPACER_RED_INVALID=5
} cspacer_reduction_algorithm_t;



/*
 *  We can register at most one plan at a time per space. 
 *  User should not change plans while a space is in an inconsistent state. 
 */

cspacer_return_t 
        cspacer_register_reduction_plan (
          cspacer_space_handle_t dest_space,      /* In: Dest Symmetric Space Handle. Such space should be allocated as a reduction space. */
          cspacer_operation_t op,                 /* In: operation associated with atomic update */
          uint64_t full_count,                    /* In: The reduction full size. Should be the same across all ranks in the team.  */
          cspacer_reduction_algorithm_t pattern); /* The reduction algorithm.*/

/*
   The reduction transfer should satisfy the declared plan.
   The order of injection decide the match of words for reduction
 */
cspacer_return_t
        cspacer_construct_reduction (
          cspacer_space_handle_t dest_space,      /* In: Dest Symmetric Space Handle. Such space should be allocated as reduction space. */
          cspacer_operation_t op,                 /* In: operation associated with atomic update */
          cspacer_channel_handle_t reduction_channel, /* In:  An ordered channel for reduction injection */
          cspacer_reduction_handle_t * ptx_hndl);  /* The reduction ordered channel. */

cspacer_return_t
    cspacer_plan_reduction(
          uint64_t full_count,                  /* In: The number elements in the reduction. */
          int32_t  chunk_size,                  /* In: The unit size for processing */
          int32_t flush_depth,           /* In: The depth of flushing requests */
          cspacer_reduction_algorithm_t pattern,/* In: the algorithm to use for reduction. */
          int32_t inplace,                      /* In: Is the reduction in place or out of place. */
          cspacer_reduction_handle_t tx_hndl);  /* Inout: The reduction handle.  */


#if COORDINATED_IMPLICIT_HANDLE
cspacer_return_t
  cspacer_deposit_reduction_chunk_threaded(
    cspacer_space_handle_t dest_space,      /* In: Dest Symmetric Space Handle. Such space should be allocated as reduction space. */
    cspacer_space_handle_t src_space,       /* In:  Src Symmetric Space Handle. 
                                               If src == dest, then there should be no gaps */
    uint64_t src_offset,                    /* In:  Offset at the source. */
    uint64_t count,                         /* In:  The transfer element count. */
    int32_t tid,
    int32_t nthreads,
    cspacer_sync_mode_t sync
    );

cspacer_return_t
    cspacer_deposit_reduction_chunk(
    cspacer_space_handle_t dest_space,      /* In: Dest Symmetric Space Handle. Such space should be allocated as reduction space. */

    cspacer_space_handle_t src_space,       /* In:  Src Symmetric Space Handle.*/
    uint64_t  src_offset,                 /* In: Offset at the source */
    uint64_t  count);                      /* In: A transfer element count. */

#else

cspacer_return_t
  cspacer_deposit_reduction_chunk_threaded(
    cspacer_space_handle_t src_space,       /* In:  Src Symmetric Space Handle. 
                                               If src == dest, then there should be no gaps */
    uint64_t src_offset,                    /* In:  Offset at the source. */
    uint64_t count,                         /* In:  The transfer element count. */
    int32_t tid,
    int32_t nthreads,
    cspacer_sync_mode_t sync,
    cspacer_reduction_handle_t tx_hndl);      /* Out: handle to memory. */

cspacer_return_t
    cspacer_deposit_reduction_chunk(
    cspacer_space_handle_t src_space,       /* In:  Src Symmetric Space Handle.*/
    uint64_t  src_offset,                 /* In: Offset at the source */
    uint64_t  count,                      /* In: A transfer element count. */
    cspacer_reduction_handle_t tx_hndl);
#endif
/*!
 * Memory handling routine
 */
#define CSPACER_DEFAULT_SPACE   0
#define CSPACER_BROADCAST_SPACE 1  /* We may need to use leader data to reduce the amount of internode data movement. */
#define CSPACER_COORDINATED_BROADCAST_SPACE 2
#define CSPACER_REDUCTION32_SPACE 3 /* We need more space for reductions  (about double the size) */
#define CSPACER_REDUCTION64_SPACE 4 /* We need more space for reductions  (about double the size) */


cspacer_return_t cspacer_sym_allocate_memory(
              cspacer_team_handle_t team,           /* In: team for symmetric allocation */
              uint64_t size,                        /* In: size of space */
              uint32_t attribute,
              cspacer_space_handle_t *space_handle);/* Out: space handle */

cspacer_return_t cspacer_select_space_tag_id(
    cspacer_space_handle_t space_handle,
    int32_t tag_id);

cspacer_return_t 
          cspacer_get_space_ptr(
              cspacer_space_handle_t handle, void **ptr);

cspacer_return_t
          cspacer_get_node_leader_space_ptr(
              cspacer_space_handle_t handle, void **ptr);

cspacer_return_t 
              cspacer_get_rank_cspace_ptr(
                                  cspacer_space_handle_t handle,int32_t smp_rank, void **ptr);
cspacer_return_t 
          cspacer_free_memory(
              cspacer_space_handle_t handle);

cspacer_return_t
       cspacer_warm_space_threaded(
                   cspacer_space_handle_t handle, 
                   int tid, 
                   int nthreads);

cspacer_return_t
       cspacer_warm_space(
                   cspacer_space_handle_t handle);

/*!
 * Team routines:
 */

#define CSPACER_SELF_TEAM       0
#define CSPACER_UNIVERSAL_TEAM  1
#define CSPACER_ROW_TEAM        2
#define CSPACER_COLUMN_TEAM     3
#define CSPACER_LAYER_TEAM      4
#define CSPACER_BLOCK_TEAM      5
#define CSPACER_TIME_TEAM       6
#define CSPACER_MIN_TEAM_COUNT  (CSPACER_UNIVERSAL_TEAM+1)

cspacer_return_t
  cspacer_get_team_count(
      int32_t *team_count);

cspacer_return_t
  cspacer_get_team_handle(
      int32_t team_id,
      cspacer_team_handle_t *team);

cspacer_return_t 
      cspacer_team_size( 
          cspacer_team_handle_t team,
          int32_t * size);

cspacer_return_t 
          cspacer_team_rank( 
          cspacer_team_handle_t team,
          int32_t * rank);

cspacer_return_t 
          cspacer_team_is_smp_leader(
          cspacer_team_handle_t team);


/* 
   Lane routines
 */
cspacer_return_t 
          cspacer_get_lane_count( 
          int32_t *lane_count);

cspacer_return_t 
          cspacer_get_lane_handle(
          int32_t lane_id,
          cspacer_lane_handle_t * handle);

cspacer_return_t cspacer_get_channel_count(
            int32_t * channel_count);


cspacer_return_t cspacer_get_channel_handle(
                        int32_t channel_id, 
                        cspacer_channel_handle_t * handle);

/*
 * General alignment requirment.
 * Could easily change with the underlying system.
 */ 

cspacer_return_t 
          cspacer_data_alignment( 
          int32_t * align);






/*
   This is a non-blocking call to initiate the transfer. 
   It has an initiate or block semantic. 
   We do not initiate or queue semantic (similar to mpi_isend).
 */



cspacer_return_t
    cspacer_initiate_transfer(
        cspacer_transfer_handle_t handle /* In: transfer handle */
        );

    /* the threaded version helps for intra node transfer */
cspacer_return_t
    cspacer_initiate_transfer_threaded(
        cspacer_transfer_handle_t handle, /* In: transfer handle */
          int tid, 
          int nthreads,
          cspacer_sync_mode_t sync);



cspacer_return_t
    cspacer_initiate_broadcast(
                cspacer_broadcast_handle_t tx_hndl);

 cspacer_return_t
    cspacer_initiate_broadcast_threaded(
        cspacer_broadcast_handle_t tx_hndl,
          int tid, 
          int nthreads,
          cspacer_sync_mode_t sync);


/*
   This is a non-blocking call to initiate the transfer. 
   It has an initiate or fail (if resources are not available) semantic. 
   A failute could indicate partial completion. As such, the runtime expects 
   the user to retry the same operation later. 
   All operations are non-cancelable by the runtime, i.e., they should be retried if they fail.
   A failure may indicate partial completion. 
   Canceling pending operations would require flushing all pending operations and user-level 
   reseting of the space state. 

 */

cspacer_return_t
    cspacer_update_transfer(
    uint64_t dest_offset,                 /* In: Aligned src offset */
    uint64_t src_offset,                 /* In: Offset at the source */
    uint64_t size,                        /* In: A transfer size. Should satisfy alignment. */
    cspacer_transfer_handle_t handle);

cspacer_return_t
    cspacer_update_broadcast(
      uint64_t dest_offset,                 /* In: Aligned src offset */
      uint64_t src_offset,                  /* In: Offset at the source */
      uint64_t size,                        /* In: A transfer size. Should satisfy alignment. */
      cspacer_broadcast_handle_t tx_hndl);

/*
 * In reduction, the order dictates the matching of the data. 
 */
cspacer_return_t
    cspacer_update_reduction(
    uint64_t  src_offset,                 /* In: Offset at the source */
    uint64_t  count,                      /* In: A transfer element count. */
    cspacer_reduction_handle_t handle);


cspacer_return_t
    cspacer_initiate_reduction(
    cspacer_reduction_handle_t handle);


cspacer_return_t
    cspacer_try_broadcast(
        cspacer_transfer_handle_t handle,
        int32_t tid,
        int32_t nthreads);



/*
  We should not free a transfer unless a lane it is injected to is flushed.
 */
cspacer_return_t 
  cspacer_free_transfer(
      cspacer_transfer_handle_t handle);

cspacer_return_t
   cspacer_free_broadcast(
           cspacer_broadcast_handle_t tx_hndl);

cspacer_return_t
   cspacer_free_coordinated_broadcast(
           cspacer_coordinated_broadcast_handle_t tx_hndl);
   
cspacer_return_t
   cspacer_free_reduction(
           cspacer_reduction_handle_t tx_hndl);


cspacer_return_t  
  cspacer_is_consistent(
    cspacer_space_handle_t  handle, 
    uint64_t tag);

cspacer_return_t  
  cspacer_is_leader_consistent(
          cspacer_space_handle_t handle, 
          uint64_t tag);

cspacer_return_t  
  cspacer_is_consistent_threaded(
          cspacer_space_handle_t  handle, 
          uint64_t tag, 
          int tid, 
          int nthreads);

cspacer_return_t  
  cspacer_is_leader_consistent_threaded(
          cspacer_space_handle_t handle, 
          uint64_t tag, 
          int tid, 
          int nthreads);



cspacer_return_t  
  cspacer_wait_consistent(
    cspacer_space_handle_t handle, 
    uint64_t tag);
/*
 * Currently OpenMP is supported. Application and runtime should agree on the OpenMP library.
 * Moreover, nthreads should match OpenMP nthreads. 
 * tid could be a virtual id that does not match openmp id. This could be used for load 
 * balancing across multiple parallel regions. 
 */

cspacer_return_t  
  cspacer_wait_consistent_threaded(
    cspacer_space_handle_t handle, 
    uint64_t tag, 
    int tid, 
    int nthreads);
  
cspacer_return_t  
  cspacer_wait_leader_consistent (
              cspacer_space_handle_t handle, 
              uint64_t tag);

cspacer_return_t  
  cspacer_wait_leader_consistent_threaded(
          cspacer_space_handle_t handle, 
          uint64_t tag, 
          int tid, 
          int nthreads);


cspacer_return_t  
  cspacer_reset_state(
    cspacer_space_handle_t handle,  /* In: Dest Symmetric Space Handle. Such space should be allocated as a reduction space. */
    cspacer_reset_type_t  reset_type); /* In: Check if peers used my copy before reseting */

void cspacer_progress_lane(cspacer_lane_handle_t lane);
 

cspacer_return_t
  cspacer_flush_lane(cspacer_lane_handle_t lane);
 
cspacer_return_t
  cspacer_flush_all_lanes(void);


cspacer_return_t
  cspacer_flush_channel(cspacer_channel_handle_t channel);


cspacer_return_t cspacer_get_lane_depth(cspacer_lane_handle_t lane, int32_t *depth);

cspacer_return_t cspacer_get_channel_depth(cspacer_channel_handle_t channel, int32_t *depth);

cspacer_return_t cspacer_declare_team_helper_threads(cspacer_team_handle_t team, int32_t nthreads);

cspacer_return_t cspacer_team_smp_barrier(cspacer_team_handle_t team);


cspacer_return_t cspacer_initiate_hw_reduction(cspacer_operation_t op, void * data);
cspacer_return_t cspacer_check_hw_reduction(void * data);

cspacer_return_t cspacer_hw_reduction(cspacer_operation_t op, void * data);


/* this is an smp barrier that should be called by all ranks within a node. */

cspacer_return_t cspacer_interoperability_enter(void);
cspacer_return_t cspacer_interoperability_exit(void);





void cspacer_barrier(void);

#ifdef __cplusplus 
} /* extern "C" */
#endif

#endif // !__CSPACER__
