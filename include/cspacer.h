/*------------------------------------------------------------------------------
 * Description: Main entrey header for the Space Consistency Runtime.
 * Copyright (c) 2019 for Khaled Z. Ibrahim <kzibrahim@lbl.gov>.
 * Terms of use are as specified in doc/license.txt.
 *------------------------------------------------------------------------------*/

#ifndef __CSPACER_H__
#define __CSPACER_H__ 
#define CSPACER     1 
#define CSPACER_VER "1.0"
#include "cspacer_pub.h"
#endif
